package cn.youfule.mecs.netty.util;

import junit.framework.TestCase;

public class CRC16UtilsTest extends TestCase {

    public static void main(String[] args) {
        byte[] bytes = CRC16Utils.CRC16(new byte[]{
                // 登录应答
                // 0x55,0x66,0x00,0x00,0x00,0x09,0x01,0x0E,0x01,0x00,0x01,0x66
                // 主动上报应答
                //0x55,0x66,0x00,0x00,0x00,0x09,0x01,0x0E,0x08,0x00,0x00
                //查询
                //0x55,0x66,0x00,0x00,0x00,0x09,0x01,0x0E,0x03,0x00,0x7F
                //0x55,0x66,0x00,0x00,0x00,0x09,0x01,0x0E,0x03,0x7F,0x83
                //A卷帘
                //0x55,0x66,0x00,0x00,0x00,0x09,0x01,0x0E,0x10,intToByte(229),0x01,0x01
                //A放帘
                //0x55,0x66,0x00,0x00,0x00,0x09,0x01,0x0E,0x10,intToByte(230),0x01,0x02
                //A卷帘/放帘 停止
                //0x55,0x66,0x00,0x00,0x00,0x09,0x01,0x0E,0x10,intToByte(231),0x01,0x03
                //上下限设置
                0x55,0x66,0x00,0x00,0x00,0x09,0x01,0x0E,0x10,0x02,0x20,
                0x00,0x01,0x00,0x01,0x00,0x01,0x00,0x01,0x00,0x01,0x00,0x01,0x00,0x01,0x00,0x01,
                0x00,0x01,0x00,0x01,0x00,0x01,0x00,0x01,0x00,0x01,0x00,0x01,0x00,0x01,0x00,0x01,


        });
        System.out.println(CRC16Utils.bytesToHexString(new byte[]{intToByte(231)}));
        System.out.println(CRC16Utils.bytesToHexString(bytes));

        byte x = intToByte(231);
        System.out.println(x &0xff);
        System.out.println(231 &0xff);
    }

    public static byte intToByte(int value) {

        return (byte) Integer.parseInt(Integer.toHexString(value), 16);
    }

}