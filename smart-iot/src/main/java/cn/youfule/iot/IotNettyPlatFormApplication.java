package cn.youfule.iot;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

//@SpringBootApplication
public class IotNettyPlatFormApplication {

    private static final Logger log = LoggerFactory.getLogger(IotNettyPlatFormApplication.class);


    public static void main(String[] args) {
        SpringApplication.run(IotNettyPlatFormApplication.class, args);
        run();
    }

    private static NettyServer nettyServer = new NettyServer();

    private static void run(){
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                nettyServer.run();
            }
        });
        thread.start();
    }

}
