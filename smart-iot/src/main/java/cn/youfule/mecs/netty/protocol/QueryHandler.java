package cn.youfule.mecs.netty.protocol;

import cn.youfule.mecs.netty.ChannelSupervise;
import cn.youfule.mecs.netty.MecsProtocol;
import cn.youfule.mecs.netty.util.Constant;
import io.netty.channel.ChannelHandlerContext;

public class QueryHandler extends AbstractHandler<QueryReportSearchInboundProtocolContext, QueryOutboundProtocolContext>{

    @Override
    protected byte[] supportFunctionCode() {
        return new byte[]{Constant.IN_FC_QUERY};
    }

    @Override
    protected void process(ChannelHandlerContext ctx, MecsProtocol<QueryReportSearchInboundProtocolContext> inbound) throws Exception {
        //TODO 进行业务处理


        //将接收到的消息添加到队列
        ChannelSupervise.setReceiveMsg(inbound);
    }

    @Override
    protected MecsProtocol<QueryOutboundProtocolContext> outboundBuild(ChannelHandlerContext ctx, MecsProtocol<QueryReportSearchInboundProtocolContext> inbound) throws Exception {
        return null;
    }
}
