package cn.youfule.mecs.netty.protocol;


import cn.youfule.mecs.netty.ProtocolField;
import lombok.Data;

import java.time.LocalDateTime;
import java.time.LocalTime;

@Data
public class QueryReportSearchInboundProtocolContext extends AbstractProtocolContext {
/*    private int startIndex;
    private int contextLength;*/

    //运行状态:Integer(8Bit) bytes[0]
    @ProtocolField(start = 0,length = 1)
    private int runState;
    //故障:Integer(32Bit) bytes[1-4]
    @ProtocolField(start = 1,length = 4)
    private int fault;
    //空气温度:Integer(16Bit) bytes[5-6]
    @ProtocolField(start = 5,length = 2)
    private int airTemperature;
    //空气湿度:Integer(16Bit) bytes[7-8]
    @ProtocolField(start = 7,length = 2)
    private int airHumidity;
    //土壤温度:Integer(16Bit) bytes[9-10]
    @ProtocolField(start = 9,length = 2)
    private int soilTemperature;
    //土壤湿度:Integer(16Bit) bytes[11-12]
    @ProtocolField(start = 11,length = 2)
    private int soilHumidity;
    //风速:Integer(16Bit) bytes[13-14]
    @ProtocolField(start = 13,length = 2)
    private int windSpeed;
    //CO2浓度:Integer(16Bit) bytes[15-16]
    @ProtocolField(start = 15,length = 2)
    private int co2Density;
    //照度:Integer(16Bit) bytes[17-18]
    @ProtocolField(start = 17,length = 2)
    private int lighting;
    //降雨量:Integer(16Bit) bytes[19-20]
    @ProtocolField(start = 19,length = 2)
    private int rainfall;
    // 非法时间起:Date(时:分) bytes[21-22]
    @ProtocolField(start = 21,length = 2)
    private LocalTime incorrectTimeBegin;
    // 非法时间止:Date(时:分) bytes[23-24]
    @ProtocolField(start = 23,length = 2)
    private LocalTime incorrectTimeEnd;
    //挂载传感器数:Integer(8Bit) bytes[25]
    @ProtocolField(start = 25,length = 1)
    private int sensorNumber;
    // 空气温度上限:Integer(16Bit) bytes[26-27]
    @ProtocolField(start = 26,length = 2)
    private int airTemperatureUpperLimit;
    // 空气温度下限:Integer(16Bit) bytes[28-29]
    @ProtocolField(start = 28,length = 2)
    private int airTemperatureLowerLimit;
    // 空气湿度上限:Integer(16Bit) bytes[30-31]
    @ProtocolField(start = 30,length = 2)
    private int airHumidityUpperLimit;
    // 空气湿度下限:Integer(16Bit) bytes[32-33]
    @ProtocolField(start = 32,length = 2)
    private int airHumidityLowerLimit;
    // 土壤温度上限:Integer(16Bit) bytes[34-35]
    @ProtocolField(start = 34,length = 2)
    private int soilTemperatureUpperLimit;
    // 土壤温度下限:Integer(16Bit) bytes[36-37]
    @ProtocolField(start = 36,length = 2)
    private int soilTemperatureLowerLimit;
    // 土壤湿度上限:Integer(16Bit) bytes[38-39]
    @ProtocolField(start = 38,length = 2)
    private int soilHumidityUpperLimit;
    // 土壤湿度下限:Integer(16Bit) bytes[40-41]
    @ProtocolField(start = 40,length = 2)
    private int soilHumidityLowerLimit;
    // 风速上限:Integer(16Bit) bytes[42-43]
    @ProtocolField(start = 42,length = 2)
    private int windSpeedUpperLimit;
    // 风速下限:Integer(16Bit) bytes[44-45]
    @ProtocolField(start = 44,length = 2)
    private int windSpeedLowerLimit;
    // CO2浓度上限:Integer(16Bit) bytes[46-47]
    @ProtocolField(start = 46,length = 2)
    private int co2DensityUpperLimit;
    // CO2浓度下限:Integer(16Bit) bytes[48-49]
    @ProtocolField(start = 48,length = 2)
    private int co2DensityLowerLimit;
    // 照度上限:Integer(16Bit) bytes[50-51]
    @ProtocolField(start = 50,length = 2)
    private int lightingUpperLimit;
    // 照度下限:Integer(16Bit) bytes[52-53]
    @ProtocolField(start = 52,length = 2)
    private int lightingLowerLimit;
    // 降雨量上限:Integer(16Bit) bytes[54-55]
    @ProtocolField(start = 54,length = 2)
    private int rainfallUpperLimit;
    // 降雨量下限:Integer(16Bit) bytes[56-57]
    @ProtocolField(start = 56,length = 2)
    private int rainfallLowerLimit;
    // 时间:Date(年-月-日 时:分:秒) bytes[58-63]
    @ProtocolField(start = 58,length = 6)
    private LocalDateTime createTime;
    //上报模式:Integer(8Bit) bytes[64]
    @ProtocolField(start = 64,length = 1)
    private int submitMode;

    //时间段1
    // 卷帘时间1:Date(时:分) bytes[65-66]
    @ProtocolField(start = 65,length = 2)
    private LocalTime blanket1OpenTime;
    // 放帘时间1:Date(时:分) bytes[67-68]
    @ProtocolField(start = 67,length = 2)
    private LocalTime blanket1CloseTime;
    // 卷膜时间1:Date(时:分) bytes[69-70]
    @ProtocolField(start = 69,length = 2)
    private LocalTime film1OpenTime;
    // 放膜时间1:Date(时:分) bytes[71-72]
    @ProtocolField(start = 71,length = 2)
    private LocalTime film1CloseTime;
    // 卷帘时间2:Date(时:分) bytes[73-74]
    @ProtocolField(start = 73,length = 2)
    private LocalTime blanket2OpenTime;
    // 放帘时间2:Date(时:分) bytes[75-76]
    @ProtocolField(start = 75,length = 2)
    private LocalTime blanket2CloseTime;
    // 卷膜时间2:Date(时:分) bytes[77-78]
    @ProtocolField(start = 77,length = 2)
    private LocalTime film2OpenTime;
    // 放膜时间2:Date(时:分) bytes[79-80]
    @ProtocolField(start = 79,length = 2)
    private LocalTime film2CloseTime;
    // 开阀时间1:Date(时:分) bytes[81-82]
    @ProtocolField(start = 81,length = 2)
    private LocalTime valve1OpenTime;
    // 关阀时间1:Date(时:分) bytes[83-84]
    @ProtocolField(start = 83,length = 2)
    private LocalTime valve1CloseTime;
    // 开阀时间2:Date(时:分) bytes[85-86]
    @ProtocolField(start = 85,length = 2)
    private LocalTime valve2OpenTime;
    // 关阀时间2:Date(时:分) bytes[87-88]
    @ProtocolField(start = 87,length = 2)
    private LocalTime valve2CloseTime;
    // 遮阳时间:Date(时:分) bytes[89-90]
    @ProtocolField(start = 89,length = 2)
    private LocalTime shadingOpenTime;
    // 透光时间:Date(时:分) bytes[91-92]
    @ProtocolField(start = 91,length = 2)
    private LocalTime shadingCloseTime;
    // 补温开启时间:Date(时:分) bytes[93-94]
    @ProtocolField(start = 93,length = 2)
    private LocalTime heatingOpenTime;
    // 补温关闭时间:Date(时:分) bytes[95-96]
    @ProtocolField(start = 95,length = 2)
    private LocalTime heatingCloseTime;
    // 补光开启时间:Date(时:分) bytes[97-98]
    @ProtocolField(start = 97,length = 2)
    private LocalTime lightingOpenTime;
    // 补光关闭时间:Date(时:分) bytes[99-100]
    @ProtocolField(start = 99,length = 2)
    private LocalTime lightingCloseTime;
    // 风机开启时间:Date(时:分) bytes[101-102]
    @ProtocolField(start = 101,length = 2)
    private LocalTime windOpenTime;
    // 风机关闭时间:Date(时:分) bytes[103-104]
    @ProtocolField(start = 103,length = 2)
    private LocalTime windCloseTime;
    // CO2开启时间:Date(时:分) bytes[105-106]
    @ProtocolField(start = 105,length = 2)
    private LocalTime co2OpenTime;
    // CO2关闭时间:Date(时:分) bytes[107-108]
    @ProtocolField(start = 107,length = 2)
    private LocalTime co2CloseTime;
    // 重启时间:Date(时:分) bytes[109-110]
    @ProtocolField(start = 109,length = 2)
    private LocalTime resetTime;
    //自动开启:Integer(8Bit) bytes[111]
    @ProtocolField(start = 111,length = 1)
    private int autoStart;

    //卷帘状态1:Integer(8Bit) [1=执行,2=停止,3=到位] bytes[112]
    @ProtocolField(start = 112,length = 1)
    private int blanket1OpenState;
    //放帘状态1:Integer(8Bit) [1=执行,2=停止,3=到位] bytes[113]
    @ProtocolField(start = 113,length = 1)
    private int blanket1CloseState;
    //卷膜状态1:Integer(8Bit) [1=执行,2=停止,3=到位] bytes[114]
    @ProtocolField(start = 114,length = 1)
    private int film1OpenState;
    //放膜状态1:Integer(8Bit) [1=执行,2=停止,3=到位] bytes[115]
    @ProtocolField(start = 115,length = 1)
    private int film1CloseState;
    //卷帘状态2:Integer(8Bit) [1=执行,2=停止,3=到位] bytes[116]
    @ProtocolField(start = 116,length = 1)
    private int blanket2OpenState;
    //放帘状态2:Integer(8Bit) [1=执行,2=停止,3=到位] bytes[117]
    @ProtocolField(start = 117,length = 1)
    private int blanket2CloseState;
    //卷膜状态2:Integer(8Bit) [1=执行,2=停止,3=到位] bytes[118]
    @ProtocolField(start = 118,length = 1)
    private int film2OpenState;
    //放膜状态2:Integer(8Bit) [1=执行,2=停止,3=到位] bytes[119]
    @ProtocolField(start = 119,length = 1)
    private int film2CloseState;
    //遮阳状态:Integer(8Bit) [1=执行,2=停止,3=到位] bytes[120]
    @ProtocolField(start = 120,length = 1)
    private int shadingOpenState;
    //透光状态:Integer(8Bit) [1=执行,2=停止,3=到位] bytes[121]
    @ProtocolField(start = 121,length = 1)
    private int shadingCloseState;
    //补温状态:Integer(8Bit) [1=开,2=关] bytes[122]
    @ProtocolField(start = 122,length = 1)
    private int heatingOpenState;
    //补光状态:Integer(8Bit) [1=开,2=关] bytes[123]
    @ProtocolField(start = 123,length = 1)
    private int lightingOpenState;
    //灌溉状态:Integer(8Bit) [1=开,2=关] bytes[124]
    @ProtocolField(start = 124,length = 1)
    private int wateringState;
    //CO2状态:Integer(8Bit) [1=开,2=关] bytes[125]
    @ProtocolField(start = 125,length = 1)
    private int co2OpenState;
    //风机状态:Integer(8Bit) [1=开,2=关] bytes[126]
    @ProtocolField(start = 126,length = 1)
    private int windOpenState;

    // 时间端二
    // 卷帘时间1:Date(时:分) bytes[127-128]
    @ProtocolField(start = 127,length = 2)
    private LocalTime blanket1OpenTime2;
    // 放帘时间1:Date(时:分) bytes[129-130]
    @ProtocolField(start = 129,length = 2)
    private LocalTime blanket1CloseTime2;
    // 卷膜时间1:Date(时:分) bytes[131-132]
    @ProtocolField(start = 131,length = 2)
    private LocalTime film1OpenTime2;
    // 放膜时间1:Date(时:分) bytes[133-134]
    @ProtocolField(start = 133,length = 2)
    private LocalTime film1CloseTime2;
    // 卷帘时间2:Date(时:分) bytes[135-136]
    @ProtocolField(start = 135,length = 2)
    private LocalTime blanket2OpenTime2;
    // 放帘时间2:Date(时:分) bytes[137-138]
    @ProtocolField(start = 137,length = 2)
    private LocalTime blanket2CloseTime2;
    // 卷膜时间2:Date(时:分) bytes[139-140]
    @ProtocolField(start = 139,length = 2)
    private LocalTime film2OpenTime2;
    // 放膜时间2:Date(时:分) bytes[141-142]
    @ProtocolField(start = 141,length = 2)
    private LocalTime film2CloseTime2;
    // 开阀时间1:Date(时:分) bytes[143-144]
    @ProtocolField(start = 143,length = 2)
    private LocalTime valve1OpenTime2;
    // 关阀时间1:Date(时:分) bytes[145-146]
    @ProtocolField(start = 145,length = 2)
    private LocalTime valve1CloseTime2;
    // 开阀时间2:Date(时:分) bytes[147-148]
    @ProtocolField(start = 147,length = 2)
    private LocalTime valve2OpenTime2;
    // 关阀时间2:Date(时:分) bytes[149-150]
    @ProtocolField(start = 149,length = 2)
    private LocalTime valve2CloseTime2;
    // 遮阳时间:Date(时:分) bytes[151-152]
    @ProtocolField(start = 151,length = 2)
    private LocalTime shadingOpenTime2;
    // 透光时间:Date(时:分) bytes[153-154]
    @ProtocolField(start = 153,length = 2)
    private LocalTime shadingCloseTime2;
    // 补温开启时间:Date(时:分) bytes[155-156]
    @ProtocolField(start = 155,length = 2)
    private LocalTime heatingOpenTime2;
    // 补温关闭时间:Date(时:分) bytes[157-158]
    @ProtocolField(start = 157,length = 2)
    private LocalTime heatingCloseTime2;
    // 补光开启时间:Date(时:分) bytes[159-160]
    @ProtocolField(start = 159,length = 2)
    private LocalTime lightingOpenTime2;
    // 补光关闭时间:Date(时:分) bytes[161-162]
    @ProtocolField(start = 161,length = 2)
    private LocalTime lightingCloseTime2;
    // 风机开启时间:Date(时:分) bytes[163-164]
    @ProtocolField(start = 163,length = 2)
    private LocalTime windOpenTime2;
    // 风机关闭时间:Date(时:分) bytes[165-166]
    @ProtocolField(start = 165,length = 2)
    private LocalTime windCloseTime2;
    // CO2开启时间:Date(时:分) bytes[167-168]
    @ProtocolField(start = 167,length = 2)
    private LocalTime co2OpenTime2;
    // CO2关闭时间:Date(时:分) bytes[169-170]
    @ProtocolField(start = 169,length = 2)
    private LocalTime co2CloseTime2;

    // 时间端三
    // 卷帘时间1:Date(时:分) bytes[171-172]
    @ProtocolField(start = 171,length = 2)
    private LocalTime blanket1OpenTime3;
    // 放帘时间1:Date(时:分) bytes[173-174]
    @ProtocolField(start = 173,length = 2)
    private LocalTime blanket1CloseTime3;
    // 卷膜时间1:Date(时:分) bytes[175-176]
    @ProtocolField(start = 175,length = 2)
    private LocalTime film1OpenTime3;
    // 放膜时间1:Date(时:分) bytes[177-178]
    @ProtocolField(start = 177,length = 2)
    private LocalTime film1CloseTime3;
    // 卷帘时间2:Date(时:分) bytes[179-180]
    @ProtocolField(start = 179,length = 2)
    private LocalTime blanket2OpenTime3;
    // 放帘时间2:Date(时:分) bytes[181-182]
    @ProtocolField(start = 181,length = 2)
    private LocalTime blanket2CloseTime3;
    // 卷膜时间2:Date(时:分) bytes[183-184]
    @ProtocolField(start = 183,length = 2)
    private LocalTime film2OpenTime3;
    // 放膜时间2:Date(时:分) bytes[185-186]
    @ProtocolField(start = 185,length = 2)
    private LocalTime film2CloseTime3;
    // 开阀时间1:Date(时:分) bytes[187-188]
    @ProtocolField(start = 187,length = 2)
    private LocalTime valve1OpenTime3;
    // 关阀时间1:Date(时:分) bytes[189-190]
    @ProtocolField(start = 189,length = 2)
    private LocalTime valve1CloseTime3;
    // 开阀时间2:Date(时:分) bytes[191-192]
    @ProtocolField(start = 191,length = 2)
    private LocalTime valve2OpenTime3;
    // 关阀时间2:Date(时:分) bytes[193-194]
    @ProtocolField(start = 193,length = 2)
    private LocalTime valve2CloseTime3;
    // 遮阳时间:Date(时:分) bytes[195-196]
    @ProtocolField(start = 195,length = 2)
    private LocalTime shadingOpenTime3;
    // 透光时间:Date(时:分) bytes[197-198]
    @ProtocolField(start = 197,length = 2)
    private LocalTime shadingCloseTime3;
    // 补温开启时间:Date(时:分) bytes[199-200]
    @ProtocolField(start = 199,length = 2)
    private LocalTime heatingOpenTime3;
    // 补温关闭时间:Date(时:分) bytes[201-202]
    @ProtocolField(start = 201,length = 2)
    private LocalTime heatingCloseTime3;
    // 补光开启时间:Date(时:分) bytes[203-204]
    @ProtocolField(start = 203,length = 2)
    private LocalTime lightingOpenTime3;
    // 补光关闭时间:Date(时:分) bytes[205-206]
    @ProtocolField(start = 205,length = 2)
    private LocalTime lightingCloseTime3;
    // 风机开启时间:Date(时:分) bytes[207-208]
    @ProtocolField(start = 207,length = 2)
    private LocalTime windOpenTime3;
    // 风机关闭时间:Date(时:分) bytes[209-210]
    @ProtocolField(start = 209,length = 2)
    private LocalTime windCloseTime3;
    // CO2开启时间:Date(时:分) bytes[211-212]
    @ProtocolField(start = 211,length = 2)
    private LocalTime co2OpenTime3;
    // CO2关闭时间:Date(时:分) bytes[213-214]
    @ProtocolField(start = 213,length = 2)
    private LocalTime co2CloseTime3;

    // 时间端四
    // 卷帘时间1:Date(时:分) bytes[215-216]
    @ProtocolField(start = 215,length = 2)
    private LocalTime blanket1OpenTime4;
    // 放帘时间1:Date(时:分) bytes[217-218]
    @ProtocolField(start = 217,length = 2)
    private LocalTime blanket1CloseTime4;
    // 卷膜时间1:Date(时:分) bytes[219-220]
    @ProtocolField(start = 219,length = 2)
    private LocalTime film1OpenTime4;
    // 放膜时间1:Date(时:分) bytes[221-222]
    @ProtocolField(start = 221,length = 2)
    private LocalTime film1CloseTime4;
    // 卷帘时间2:Date(时:分) bytes[223-224]
    @ProtocolField(start = 223,length = 2)
    private LocalTime blanket2OpenTime4;
    // 放帘时间2:Date(时:分) bytes[225-226]
    @ProtocolField(start = 225,length = 2)
    private LocalTime blanket2CloseTime4;
    // 卷膜时间2:Date(时:分) bytes[227-228]
    @ProtocolField(start = 227,length = 2)
    private LocalTime film2OpenTime4;
    // 放膜时间2:Date(时:分) bytes[229-230]
    @ProtocolField(start = 229,length = 2)
    private LocalTime film2CloseTime4;
    // 开阀时间1:Date(时:分) bytes[231-232]
    @ProtocolField(start = 231,length = 2)
    private LocalTime valve1OpenTime4;
    // 关阀时间1:Date(时:分) bytes[233-234]
    @ProtocolField(start = 233,length = 2)
    private LocalTime valve1CloseTime4;
    // 开阀时间2:Date(时:分) bytes[235-236]
    @ProtocolField(start = 235,length = 2)
    private LocalTime valve2OpenTime4;
    // 关阀时间2:Date(时:分) bytes[237-238]
    @ProtocolField(start = 237,length = 2)
    private LocalTime valve2CloseTime4;
    // 遮阳时间:Date(时:分) bytes[239-240]
    @ProtocolField(start = 239,length = 2)
    private LocalTime shadingOpenTime4;
    // 透光时间:Date(时:分) bytes[241-242]
    @ProtocolField(start = 241,length = 2)
    private LocalTime shadingCloseTime4;
    // 补温开启时间:Date(时:分) bytes[243-244]
    @ProtocolField(start = 243,length = 2)
    private LocalTime heatingOpenTime4;
    // 补温关闭时间:Date(时:分) bytes[245-246]
    @ProtocolField(start = 245,length = 2)
    private LocalTime heatingCloseTime4;
    // 补光开启时间:Date(时:分) bytes[247-248]
    @ProtocolField(start = 247,length = 2)
    private LocalTime lightingOpenTime4;
    // 补光关闭时间:Date(时:分) bytes[249-250]
    @ProtocolField(start = 249,length = 2)
    private LocalTime lightingCloseTime4;
    // 风机开启时间:Date(时:分) bytes[251-252]
    @ProtocolField(start = 251,length = 2)
    private LocalTime windOpenTime4;
    // 风机关闭时间:Date(时:分) bytes[253-254]
    @ProtocolField(start = 253,length = 2)
    private LocalTime windCloseTime4;
    // CO2开启时间:Date(时:分) bytes[255-256]
    @ProtocolField(start = 255,length = 2)
    private LocalTime co2OpenTime4;
    // CO2关闭时间:Date(时:分) bytes[257-258]
    @ProtocolField(start = 257,length = 2)
    private LocalTime co2CloseTime4;

}
