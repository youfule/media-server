package cn.youfule.mecs.netty.protocol;

import cn.youfule.mecs.netty.MecsProtocol;
import cn.youfule.mecs.netty.util.Constant;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;

public class HeartbeatDecoder extends AbstractDecoder<HeartbeatInboundProtocolContext>{

    @Override
    protected byte[] supportFunctionCode() {
        return new byte[]{Constant.IN_FC_HEARTBEAT};
    }

    @Override
    protected HeartbeatInboundProtocolContext decodeContext(ChannelHandlerContext channelHandlerContext, MecsProtocol<HeartbeatInboundProtocolContext> prot, ByteBuf buffer) {
        return null;
    }

    @Override
    protected int supportMinLength() {
        return Constant.HEARTBEAT_BUF_MIN_LEN;
    }
}
