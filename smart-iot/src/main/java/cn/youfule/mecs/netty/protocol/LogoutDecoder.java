package cn.youfule.mecs.netty.protocol;

import cn.youfule.mecs.netty.util.Constant;

public class LogoutDecoder extends AbstractDecoder<LogoutInboundProtocolContext>{

    @Override
    protected byte[] supportFunctionCode() {
        return new byte[]{Constant.IN_FC_LOGOUT};
    }

/*    @Override
    protected LogoutInboundProtocolContext decodeContext(ChannelHandlerContext channelHandlerContext, MecsProtocol<LogoutInboundProtocolContext> prot, ByteBuf buffer) {
        LogoutInboundProtocolContext context = new LogoutInboundProtocolContext();
        context.setStartIndex(buffer.readByte());
        context.setContextLength(buffer.readByte());
        return context;
    }*/

    @Override
    protected int supportMinLength() {
        return Constant.LOGOUT_BUF_MIN_LEN;
    }
}
