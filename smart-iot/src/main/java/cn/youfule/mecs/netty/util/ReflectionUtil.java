package cn.youfule.mecs.netty.util;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

public class ReflectionUtil {

    public static Class<?> getTypeArgumentClassList(Class<?> genericClass) {
        return getTypeArgumentClassList(genericClass,0);
    }
    public static Class<?> getTypeArgumentClassList(Class<?> genericClass,int index) {
        Type rawType = genericClass.getGenericSuperclass();
        ParameterizedType o = (ParameterizedType) rawType;

        Type[] typeArguments = o.getActualTypeArguments();
        if(typeArguments.length <= index){
            throw new RuntimeException("获取泛型越界");
        }
        return (Class<?>) typeArguments[index];
    }
}
