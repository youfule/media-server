package cn.youfule.mecs.netty;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

/**
 * 项目初始化
 * @author wunaozai
 * @date 2018-05-24
 */
@Component
@Slf4j
public class OnStartListener implements ApplicationListener<ContextRefreshedEvent> {


    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        ApplicationContext parent = event.getApplicationContext().getParent();
        if(parent==null) {
            log.info("Run on Start Listener.");
            new Thread(() -> {
                try {
                    new NettyServer().run(5000);
                } catch (Exception e) {
                    e.printStackTrace();

                }
            }).start();
        }
    }

}
