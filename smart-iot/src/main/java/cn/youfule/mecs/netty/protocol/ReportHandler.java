package cn.youfule.mecs.netty.protocol;

import cn.youfule.mecs.netty.MecsProtocol;
import cn.youfule.mecs.netty.util.Constant;
import io.netty.channel.ChannelHandlerContext;

public class ReportHandler extends AbstractHandler<QueryReportSearchInboundProtocolContext, ReportOutboundProtocolContext>{

    @Override
    protected byte[] supportFunctionCode() {
        return new byte[]{Constant.IN_FC_REPORT};
    }

    @Override
    protected void process(ChannelHandlerContext ctx, MecsProtocol<QueryReportSearchInboundProtocolContext> inbound) throws Exception {
        //TODO 进行业务处理

        // 写入数据库
        // GhSocketRecordService.DataWriting(ip,props,state,number);
        // 日志写入库
        // GhSocketRecordService.LogWriting(ip, "10031", "5", "数据入库，共 {"+data.length+"} 字节数据", CRC16Utils.bytesToHexString(data), "写入成功");
        // 打印
        // outPrint(data, "查询上报:data= %s, 共 %s 字节");
    }

    @Override
    protected ReportOutboundProtocolContext outboundContextBuild(ChannelHandlerContext ctx, MecsProtocol<QueryReportSearchInboundProtocolContext> inbound, MecsProtocol<ReportOutboundProtocolContext> outbound) throws Exception {
        ReportOutboundProtocolContext context = new ReportOutboundProtocolContext();
        context.setStartIndex(0);
        context.setContextLength(0);
        return context;
    }
}
