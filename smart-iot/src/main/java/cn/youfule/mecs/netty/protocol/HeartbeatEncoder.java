package cn.youfule.mecs.netty.protocol;

import cn.youfule.mecs.netty.util.Constant;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;

public class HeartbeatEncoder extends AbstractEncoder<HeartbeatOutboundProtocolContext>{

    @Override
    protected byte[] supportFunctionCode() {
        return new byte[]{Constant.OUT_FC_HEARTBEAT};
    }

    @Override
    protected void encodeContext(ChannelHandlerContext channelHandlerContext, HeartbeatOutboundProtocolContext prot, ByteBuf buffer) {

    }
}
