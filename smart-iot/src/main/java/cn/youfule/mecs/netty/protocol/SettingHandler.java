package cn.youfule.mecs.netty.protocol;

import cn.youfule.mecs.netty.ChannelSupervise;
import cn.youfule.mecs.netty.MecsProtocol;
import cn.youfule.mecs.netty.util.Constant;
import io.netty.channel.ChannelHandlerContext;

public class SettingHandler extends AbstractHandler<SettingInboundProtocolContext, SettingOutboundProtocolContext>{
    @Override
    protected byte[] supportFunctionCode() {
        return new byte[]{Constant.IN_FC_SETTING};
    }

    @Override
    protected void process(ChannelHandlerContext ctx, MecsProtocol<SettingInboundProtocolContext> inbound) throws Exception {
        //将接收到的消息添加到队列
        ChannelSupervise.setReceiveMsg(inbound);
    }

    @Override
    protected MecsProtocol<SettingOutboundProtocolContext> outboundBuild(ChannelHandlerContext ctx, MecsProtocol<SettingInboundProtocolContext> inbound) throws Exception {
        return null;
    }
}
