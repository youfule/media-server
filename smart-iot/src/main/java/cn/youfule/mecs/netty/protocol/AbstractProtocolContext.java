package cn.youfule.mecs.netty.protocol;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class AbstractProtocolContext {

        public int startIndex;
        public int contextLength;
}
