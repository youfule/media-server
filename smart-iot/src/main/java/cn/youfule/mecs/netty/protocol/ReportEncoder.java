package cn.youfule.mecs.netty.protocol;

import cn.youfule.mecs.netty.util.Constant;

public class ReportEncoder extends AbstractEncoder<ReportOutboundProtocolContext>{

    @Override
    protected byte[] supportFunctionCode() {
        return new byte[]{Constant.OUT_FC_REPORT};
    }

/*    @Override
    protected void encodeContext(ChannelHandlerContext channelHandlerContext, ReportOutboundProtocolContext prot, ByteBuf buffer) {
        buffer.writeByte(prot.getStartIndex());
        buffer.writeByte(prot.getContextLength());
    }*/
}
