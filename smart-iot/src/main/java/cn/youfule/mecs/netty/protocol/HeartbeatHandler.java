package cn.youfule.mecs.netty.protocol;

import cn.youfule.mecs.netty.MecsProtocol;
import cn.youfule.mecs.netty.util.Constant;
import io.netty.channel.ChannelHandlerContext;

public class HeartbeatHandler extends AbstractHandler<HeartbeatInboundProtocolContext, HeartbeatOutboundProtocolContext>{

    @Override
    protected byte[] supportFunctionCode() {
        return new byte[]{Constant.IN_FC_HEARTBEAT};
    }

    @Override
    protected HeartbeatOutboundProtocolContext outboundContextBuild(ChannelHandlerContext ctx, MecsProtocol<HeartbeatInboundProtocolContext> inbound, MecsProtocol<HeartbeatOutboundProtocolContext> outbound) throws Exception {
        outbound.setFunctionCode(Constant.OUT_FC_HEARTBEAT);
        return null;
    }
}
