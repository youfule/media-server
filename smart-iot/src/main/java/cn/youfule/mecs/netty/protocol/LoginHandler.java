package cn.youfule.mecs.netty.protocol;

import cn.youfule.mecs.netty.MecsProtocol;
import cn.youfule.mecs.netty.util.Constant;
import io.netty.channel.ChannelHandlerContext;

public class LoginHandler extends AbstractHandler<LoginInboundProtocolContext, LoginOutboundProtocolContext>{
    @Override
    protected byte[] supportFunctionCode() {
        return new byte[]{Constant.IN_FC_LOGIN};
    }

    @Override
    protected LoginOutboundProtocolContext outboundContextBuild(ChannelHandlerContext ctx, MecsProtocol<LoginInboundProtocolContext> inbound, MecsProtocol<LoginOutboundProtocolContext> outbound) throws Exception {
        LoginOutboundProtocolContext context = new LoginOutboundProtocolContext();
        context.setStartIndex(0);
        context.setContextLength(1);
        context.setContext(0x66);
        return context;
    }
}
