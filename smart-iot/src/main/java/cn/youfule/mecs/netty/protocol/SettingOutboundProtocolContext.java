package cn.youfule.mecs.netty.protocol;

import cn.youfule.mecs.netty.ProtocolField;
import lombok.Data;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;
import java.time.LocalTime;

@Data
@Accessors(chain = true)
public class SettingOutboundProtocolContext extends AbstractProtocolContext{
/*    private int startIndex;
    private int contextLength;*/

    //其他数据

    @ProtocolField(start = 0,length = 1)
    private int operation;// 执行动作:Integer(8Bit) bytes[1]
    @ProtocolField(start = 1,length = 2)
    private int airTemperatureUpperLimit; // 空气温度上限:Integer(16Bit) bytes[2-3]
    @ProtocolField(start = 3,length = 2)
    private int airTemperatureLowerLimit; // 空气温度下限:Integer(16Bit) bytes[4-5]
    @ProtocolField(start = 5,length = 2)
    private int airHumidityUpperLimit; // 空气湿度上限:Integer(16Bit) bytes[6-7]
    @ProtocolField(start = 7,length = 2)
    private int airHumidityLowerLimit; // 空气湿度下限:Integer(16Bit) bytes[8-9]
    @ProtocolField(start = 9,length = 2)
    private int soilTemperatureUpperLimit; // 土壤温度上限:Integer(16Bit) bytes[10-11]
    @ProtocolField(start = 11,length = 2)
    private int soilTemperatureLowerLimit; // 土壤温度下限:Integer(16Bit) bytes[12-13]
    @ProtocolField(start = 13,length = 2)
    private int soilHumidityUpperLimit; // 土壤湿度上限:Integer(16Bit) bytes[14-15]
    @ProtocolField(start = 15,length = 2)
    private int soilHumidityLowerLimit; // 土壤湿度下限:Integer(16Bit) bytes[16-17]
    @ProtocolField(start = 17,length = 2)
    private int windSpeedUpperLimit; // 风速上限:Integer(16Bit) bytes[18-19]
    @ProtocolField(start = 19,length = 2)
    private int windSpeedLowerLimit; // 风速下限:Integer(16Bit) bytes[20-21]
    @ProtocolField(start = 21,length = 2)
    private int co2DensityUpperLimit; // CO2浓度上限:Integer(16Bit) bytes[22-23]
    @ProtocolField(start = 23,length = 2)
    private int co2DensityLowerLimit; // CO2浓度下限:Integer(16Bit) bytes[24-25]
    @ProtocolField(start = 25,length = 2)
    private int lightingUpperLimit; // 照度上限:Integer(16Bit) bytes[26-27]
    @ProtocolField(start = 27,length = 2)
    private int lightingLowerLimit; // 照度下限:Integer(16Bit) bytes[28-29]
    @ProtocolField(start = 29,length = 2)
    private int rainfallUpperLimit; // 降雨量上限:Integer(16Bit) bytes[30-31]
    @ProtocolField(start = 31,length = 2)
    private int rainfallLowerLimit; // 降雨量下限:Integer(16Bit) bytes[32-33]
    @ProtocolField(start = 33,length = 2)
    private LocalTime incorrectTimeBegin; // 非法时间起:Date(时:分) bytes[34-35]
    @ProtocolField(start = 35,length = 2)
    private LocalTime incorrectTimeEnd; // 非法时间止:Date(时:分) bytes[36-37]
    @ProtocolField(start = 37,length = 2)
    private int lockCmd; // 锁机命令:Integer(16Bit) bytes[38-39]
    @ProtocolField(start = 39,length = 4)
    private int unlockPassword; // 解锁密码:Integer(32Bit) bytes[40-43]
    @ProtocolField(start = 43,length = 6)
    private LocalDateTime createTime;// 时间:Date(年-月-日 时:分:秒) bytes[44-49]
    @ProtocolField(start = 49,length = 1)
    private int submitMode; //上报模式:Integer(8Bit) bytes[50]

    /////////////////////////////////////////////////////////////////
    @ProtocolField(start = 50,length = 2)
    private LocalTime blanket1OpenTime; // A卷帘时间:Date(时:分) bytes[51-52]
    @ProtocolField(start = 52,length = 2)
    private LocalTime blanket1CloseTime; // A放帘时间:Date(时:分) bytes[53-54]
    @ProtocolField(start = 54,length = 2)
    private LocalTime film1OpenTime; // A卷膜时间:Date(时:分) bytes[55-56]
    @ProtocolField(start = 56,length = 2)
    private LocalTime film1CloseTime; // A放膜时间:Date(时:分) bytes[57-58]
    @ProtocolField(start = 58,length = 2)
    private LocalTime blanket2OpenTime; // B卷帘时间:Date(时:分) bytes[59-60]
    @ProtocolField(start = 60,length = 2)
    private LocalTime blanket2CloseTime; // B放帘时间:Date(时:分) bytes[61-62]
    @ProtocolField(start = 62,length = 2)
    private LocalTime film2OpenTime; // B卷膜时间:Date(时:分) bytes[63-64]
    @ProtocolField(start = 64,length = 2)
    private LocalTime film2CloseTime; // B放膜时间:Date(时:分) bytes[65-66]
    @ProtocolField(start = 66,length = 2)
    private LocalTime valve1OpenTime; // A开阀时间:Date(时:分) bytes[67-68]
    @ProtocolField(start = 68,length = 2)
    private LocalTime valve1CloseTime; // A关阀时间:Date(时:分) bytes[69-70]
    @ProtocolField(start = 70,length = 2)
    private LocalTime valve2OpenTime; // B开阀时间:Date(时:分) bytes[71-72]
    @ProtocolField(start = 72,length = 2)
    private LocalTime valve2CloseTime; // B关阀时间:Date(时:分) bytes[73-74]
    @ProtocolField(start = 74,length = 2)
    private LocalTime shadingOpenTime; // 遮阳时间:Date(时:分) bytes[75-76]
    @ProtocolField(start = 76,length = 2)
    private LocalTime shadingCloseTime; // 透光时间:Date(时:分) bytes[77-78]
    @ProtocolField(start = 78,length = 2)
    private LocalTime heatingOpenTime; // 补温开启时间:Date(时:分) bytes[79-80]
    @ProtocolField(start = 80,length = 2)
    private LocalTime heatingCloseTime; // 补温关闭时间:Date(时:分) bytes[81-82]
    @ProtocolField(start = 82,length = 2)
    private LocalTime lightingOpenTime; // 补光开启时间:Date(时:分) bytes[83-84]
    @ProtocolField(start = 84,length = 2)
    private LocalTime lightingCloseTime; // 补光关闭时间:Date(时:分) bytes[85-86]
    @ProtocolField(start = 86,length = 2)
    private LocalTime windOpenTime; // 风机开启时间:Date(时:分) bytes[87-88]
    @ProtocolField(start = 88,length = 2)
    private LocalTime windCloseTime; // 风机关闭时间:Date(时:分) bytes[89-90]
    @ProtocolField(start = 90,length = 2)
    private LocalTime co2OpenTime; // CO2开启时间:Date(时:分) bytes[91-92]
    @ProtocolField(start = 92,length = 2)
    private LocalTime co2CloseTime; // CO2关闭时间:Date(时:分) bytes[93-94]
    // 重启时间:Date(时:分) bytes[95-96]
    @ProtocolField(start = 94,length = 2)
    private LocalTime resetTime;
    //自动开启:Integer(8Bit) bytes[97]
    @ProtocolField(start = 96,length = 1)
    private int autoStart;
    ////////////////////////////////////////////////////////////////////////
    @ProtocolField(start = 97,length = 2)
    private LocalTime blanket1OpenTime2; // A卷帘时间2:Date(时:分) bytes[98-99]
    @ProtocolField(start = 99,length = 2)
    private LocalTime blanket1CloseTime2; // A放帘时间2:Date(时:分) bytes[100-101]
    @ProtocolField(start = 101,length = 2)
    private LocalTime film1OpenTime2; // A卷膜时间2:Date(时:分) bytes[102-103]
    @ProtocolField(start = 103,length = 2)
    private LocalTime film1CloseTime2; // A放膜时间2:Date(时:分) bytes[104-105]
    @ProtocolField(start = 105,length = 2)
    private LocalTime blanket2OpenTime2; // B卷帘时间2:Date(时:分) bytes[106-107]
    @ProtocolField(start = 107,length = 2)
    private LocalTime blanket2CloseTime2; // B放帘时间2:Date(时:分) bytes[108-109]
    @ProtocolField(start = 109,length = 2)
    private LocalTime film2OpenTime2; // B卷膜时间2:Date(时:分) bytes[110-111]
    @ProtocolField(start = 111,length = 2)
    private LocalTime film2CloseTime2; // B放膜时间2:Date(时:分) bytes[112-113]
    @ProtocolField(start = 113,length = 2)
    private LocalTime valve1OpenTime2; // A开阀时间2:Date(时:分) bytes[114-115]
    @ProtocolField(start = 115,length = 2)
    private LocalTime valve1CloseTime2; // A关阀时间2:Date(时:分) bytes[116-117]
    @ProtocolField(start = 117,length = 2)
    private LocalTime valve2OpenTime2; // B开阀时间2:Date(时:分) bytes[118-119]
    @ProtocolField(start = 119,length = 2)
    private LocalTime valve2CloseTime2; // B关阀时间2:Date(时:分) bytes[120-121]
    @ProtocolField(start = 121,length = 2)
    private LocalTime shadingOpenTime2; // 遮阳时间:Date(时:分) bytes[122-123]
    @ProtocolField(start = 123,length = 2)
    private LocalTime shadingCloseTime2; // 透光时间:Date(时:分) bytes[124-125]
    @ProtocolField(start = 125,length = 2)
    private LocalTime heatingOpenTime2; // 补温开启时间:Date(时:分) bytes[126-127]
    @ProtocolField(start = 127,length = 2)
    private LocalTime heatingCloseTime2; // 补温关闭时间:Date(时:分) bytes[128-129]
    @ProtocolField(start = 129,length = 2)
    private LocalTime lightingOpenTime2; // 补光开启时间:Date(时:分) bytes[130-131]
    @ProtocolField(start = 131,length = 2)
    private LocalTime lightingCloseTime2; // 补光关闭时间:Date(时:分) bytes[132-133]
    @ProtocolField(start = 133,length = 2)
    private LocalTime windOpenTime2; // 风机开启时间:Date(时:分) bytes[134-135]
    @ProtocolField(start = 135,length = 2)
    private LocalTime windCloseTime2; // 风机关闭时间:Date(时:分) bytes[136-137]
    @ProtocolField(start = 137,length = 2)
    private LocalTime co2OpenTime2; // CO2开启时间:Date(时:分) bytes[138-139]
    @ProtocolField(start = 139,length = 2)
    private LocalTime co2CloseTime2; // CO2关闭时间:Date(时:分) bytes[140-141]
    ////////////////////////////////////////////////////////////////////////
    @ProtocolField(start = 141,length = 2)
    private LocalTime blanket1OpenTime3; // A卷帘时间3:Date(时:分) bytes[142-143]
    @ProtocolField(start = 143,length = 2)
    private LocalTime blanket1CloseTime3; // A放帘时间3:Date(时:分) bytes[144-145]
    @ProtocolField(start = 145,length = 2)
    private LocalTime film1OpenTime3; // A卷膜时间3:Date(时:分) bytes[146-147]
    @ProtocolField(start = 147,length = 2)
    private LocalTime film1CloseTime3; // A放膜时间3:Date(时:分) bytes[148-149]
    @ProtocolField(start = 149,length = 2)
    private LocalTime blanket2OpenTime3; // B卷帘时间3:Date(时:分) bytes[150-151]
    @ProtocolField(start = 151,length = 2)
    private LocalTime blanket2CloseTime3; // B放帘时间3:Date(时:分) bytes[152-153]
    @ProtocolField(start = 153,length = 2)
    private LocalTime film2OpenTime3; // B卷膜时间3:Date(时:分) bytes[154-155]
    @ProtocolField(start = 155,length = 2)
    private LocalTime film2CloseTime3; // B放膜时间3:Date(时:分) bytes[156-157]
    @ProtocolField(start = 157,length = 2)
    private LocalTime valve1OpenTime3; // A开阀时间3:Date(时:分) bytes[158-159]
    @ProtocolField(start = 159,length = 2)
    private LocalTime valve1CloseTime3; // A关阀时间3:Date(时:分) bytes[160-161]
    @ProtocolField(start = 161,length = 2)
    private LocalTime valve2OpenTime3; // B开阀时间3:Date(时:分) bytes[162-163]
    @ProtocolField(start = 163,length = 2)
    private LocalTime valve2CloseTime3; // B关阀时间3:Date(时:分) bytes[164-165]
    @ProtocolField(start = 165,length = 2)
    private LocalTime shadingOpenTime3; // 遮阳时间:Date(时:分) bytes[166-167]
    @ProtocolField(start = 167,length = 2)
    private LocalTime shadingCloseTime3; // 透光时间:Date(时:分) bytes[168-169]
    @ProtocolField(start = 169,length = 2)
    private LocalTime heatingOpenTime3; // 补温开启时间:Date(时:分) bytes[170-171]
    @ProtocolField(start = 171,length = 2)
    private LocalTime heatingCloseTime3; // 补温关闭时间:Date(时:分) bytes[172-173]
    @ProtocolField(start = 173,length = 2)
    private LocalTime lightingOpenTime3; // 补光开启时间:Date(时:分) bytes[174-175]
    @ProtocolField(start = 175,length = 2)
    private LocalTime lightingCloseTime3; // 补光关闭时间:Date(时:分) bytes[176-177]
    @ProtocolField(start = 177,length = 2)
    private LocalTime windOpenTime3; // 风机开启时间:Date(时:分) bytes[178-179]
    @ProtocolField(start = 179,length = 2)
    private LocalTime windCloseTime3; // 风机关闭时间:Date(时:分) bytes[180-181]
    @ProtocolField(start = 181,length = 2)
    private LocalTime co2OpenTime3; // CO2开启时间:Date(时:分) bytes[182-183]
    @ProtocolField(start = 183,length = 2)
    private LocalTime co2CloseTime3; // CO2关闭时间:Date(时:分) bytes[184-185]
    //////////////////////////////////////////////////////////////////////////
    @ProtocolField(start = 185,length = 2)
    private LocalTime blanket1OpenTime4; // A卷帘时间4:Date(时:分) bytes[186-187]
    @ProtocolField(start = 187,length = 2)
    private LocalTime blanket1CloseTime4; // A放帘时间4:Date(时:分) bytes[188-189]
    @ProtocolField(start = 189,length = 2)
    private LocalTime film1OpenTime4; // A卷膜时间4:Date(时:分) bytes[190-191]
    @ProtocolField(start = 191,length = 2)
    private LocalTime film1CloseTime4; // A放膜时间4:Date(时:分) bytes[192-193]
    @ProtocolField(start = 193,length = 2)
    private LocalTime blanket2OpenTime4; // B卷帘时间4:Date(时:分) bytes[194-195]
    @ProtocolField(start = 195,length = 2)
    private LocalTime blanket2CloseTime4; // B放帘时间4:Date(时:分) bytes[196-197]
    @ProtocolField(start = 197,length = 2)
    private LocalTime film2OpenTime4; // B卷膜时间4:Date(时:分) bytes[198-199]
    @ProtocolField(start = 199,length = 2)
    private LocalTime film2CloseTime4; // B放膜时间4:Date(时:分) bytes[200-201]
    @ProtocolField(start = 201,length = 2)
    private LocalTime valve1OpenTime4; // A开阀时间4:Date(时:分) bytes[202-203]
    @ProtocolField(start = 203,length = 2)
    private LocalTime valve1CloseTime4; // A关阀时间4:Date(时:分) bytes[204-205]
    @ProtocolField(start = 205,length = 2)
    private LocalTime valve2OpenTime4; // B开阀时间4:Date(时:分) bytes[206-207]
    @ProtocolField(start = 207,length = 2)
    private LocalTime valve2CloseTime4; // B关阀时间4:Date(时:分) bytes[208-209]
    @ProtocolField(start = 209,length = 2)
    private LocalTime shadingOpenTime4; // 遮阳时间:Date(时:分) bytes[210-211]
    @ProtocolField(start = 211,length = 2)
    private LocalTime shadingCloseTime4; // 透光时间:Date(时:分) bytes[212-213]
    @ProtocolField(start = 213,length = 2)
    private LocalTime heatingOpenTime4; // 补温开启时间:Date(时:分) bytes[214-215]
    @ProtocolField(start = 215,length = 2)
    private LocalTime heatingCloseTime4; // 补温关闭时间:Date(时:分) bytes[216-217]
    @ProtocolField(start = 217,length = 2)
    private LocalTime lightingOpenTime4; // 补光开启时间:Date(时:分) bytes[218-219]
    @ProtocolField(start = 219,length = 2)
    private LocalTime lightingCloseTime4; // 补光关闭时间:Date(时:分) bytes[220-221]
    @ProtocolField(start = 221,length = 2)
    private LocalTime windOpenTime4; // 风机开启时间:Date(时:分) bytes[222-223]
    @ProtocolField(start = 223,length = 2)
    private LocalTime windCloseTime4; // 风机关闭时间:Date(时:分) bytes[224-225]
    @ProtocolField(start = 225,length = 2)
    private LocalTime co2OpenTime4; // CO2开启时间:Date(时:分) bytes[226-227]
    @ProtocolField(start = 227,length = 2)
    private LocalTime co2CloseTime4; // CO2关闭时间:Date(时:分) bytes[228-229]

    //A卷帘:Integer(8Bit) operation=1 bytes[230]
    @ProtocolField(start = 229,length = 1)
    private int blanket1Open;
    //A放帘:Integer(8Bit) operation=2 bytes[231]
    @ProtocolField(start = 230,length = 1)
    private int blanket1Close;
    //A卷帘/放帘 停止:Integer(8Bit) operation=3 bytes[232]
    @ProtocolField(start = 231,length = 1)
    private int blanket1Stop;
    //A卷膜:Integer(8Bit) operation=4 bytes[233]
    @ProtocolField(start = 232,length = 1)
    private int film1Open;
    //A放膜:Integer(8Bit) operation=5 bytes[234]
    @ProtocolField(start = 233,length = 1)
    private int film1Close;
    //A卷膜/放膜 停止:Integer(8Bit) operation=6 bytes[235]
    @ProtocolField(start = 234,length = 1)
    private int film1Stop;
    //B卷膜:Integer(8Bit) operation=7 bytes[236]
    @ProtocolField(start = 235,length = 1)
    private int film2Open;
    //B放膜:Integer(8Bit) operation=8 bytes[237]
    @ProtocolField(start = 236,length = 1)
    private int film2Close;
    //B卷膜/放膜 停止:Integer(8Bit) operation=9 bytes[238]
    @ProtocolField(start = 237,length = 1)
    private int film2Stop;
    //B卷帘:Integer(8Bit) operation=10 bytes[239]
    @ProtocolField(start = 238,length = 1)
    private int blanket2Open;
    //B放帘:Integer(8Bit) operation=11 bytes[240]
    @ProtocolField(start = 239,length = 1)
    private int blanket2Close;
    //B卷帘/放帘 停止:Integer(8Bit) operation=12 bytes[241]
    @ProtocolField(start = 240,length = 1)
    private int blanket2Stop;
    //遮阳:Integer(8Bit) operation=13 bytes[242]
    @ProtocolField(start = 241,length = 1)
    private int shadingOpen;
    //透光:Integer(8Bit) operation=14 bytes[243]
    @ProtocolField(start = 242,length = 1)
    private int shadingClose;
    //遮阳/透光 停止:Integer(8Bit) operation=15 bytes[244]
    @ProtocolField(start = 243,length = 1)
    private int shadingStop;
    //补温开:Integer(8Bit) operation=16 bytes[245]
    @ProtocolField(start = 244,length = 1)
    private int heatingOpen;
    //补温关:Integer(8Bit) operation=17 bytes[246]
    @ProtocolField(start = 245,length = 1)
    private int heatingClose;
    //CO2开:Integer(8Bit) operation=18 bytes[247]
    @ProtocolField(start = 246,length = 1)
    private int co2Open;
    //CO2关:Integer(8Bit) operation=19 bytes[248]
    @ProtocolField(start = 247,length = 1)
    private int co2Close;
    //补光开:Integer(8Bit) operation=20 bytes[249]
    @ProtocolField(start = 248,length = 1)
    private int lightingOpen;
    //补光关:Integer(8Bit) operation=21 bytes[250]
    @ProtocolField(start = 249,length = 1)
    private int lightingClose;
    //灌溉/施肥 开:Integer(8Bit) operation=22 bytes[251]
    @ProtocolField(start = 250,length = 1)
    private int wateringOpen;
    //灌溉/施肥 关:Integer(8Bit) operation=23 bytes[252]
    @ProtocolField(start = 251,length = 1)
    private int wateringClose;
    //排风 开:Integer(8Bit) operation=24 bytes[253]
    @ProtocolField(start = 252,length = 1)
    private int windOpen;
    //排风 关:Integer(8Bit) operation=25 bytes[254]
    @ProtocolField(start = 253,length = 1)
    private int windClose;

}
