package cn.youfule.mecs.netty.protocol;

import cn.youfule.mecs.netty.ChannelSupervise;
import cn.youfule.mecs.netty.MecsProtocol;
import cn.youfule.mecs.netty.util.Constant;
import io.netty.channel.ChannelHandlerContext;

public class UpgradeHandler extends AbstractHandler<UpgradeInboundProtocolContext, UpgradeOutboundProtocolContext>{

    @Override
    protected byte[] supportFunctionCode() {
        return new byte[]{Constant.IN_FC_UPGRADE};
    }

    @Override
    protected void process(ChannelHandlerContext ctx, MecsProtocol<UpgradeInboundProtocolContext> inbound) throws Exception {
        //将接收到的消息添加到队列
        ChannelSupervise.setReceiveMsg(inbound);
    }

    @Override
    protected MecsProtocol<UpgradeOutboundProtocolContext> outboundBuild(ChannelHandlerContext ctx, MecsProtocol<UpgradeInboundProtocolContext> inbound) throws Exception {
        return null;
    }
}
