package cn.youfule.mecs.netty.protocol;

import cn.youfule.mecs.netty.util.Constant;
import io.netty.buffer.ByteBuf;

import java.time.LocalDateTime;
import java.time.LocalTime;

public class SettingEncoder extends AbstractEncoder<SettingOutboundProtocolContext>{

    @Override
    protected byte[] supportFunctionCode() {
        return new byte[]{Constant.OUT_FC_SETTING};
    }

/*
    @Override
    protected void encodeContext(ChannelHandlerContext channelHandlerContext, SettingOutboundProtocolContext prot, ByteBuf buffer) {
        buffer.writeByte(prot.getStartIndex());
        buffer.writeByte(prot.getContextLength());

        ByteBuf buf = Unpooled.buffer();
        //TODO 写入其他数据
        buf.writeByte(prot.getOperation());
        buf.writeShort(prot.getAirTemperatureUpperLimit());
        buf.writeShort(prot.getAirTemperatureLowerLimit());
        buf.writeShort(prot.getAirHumidityUpperLimit());
        buf.writeShort(prot.getAirHumidityLowerLimit());
        buf.writeShort(prot.getSoilTemperatureUpperLimit());
        buf.writeShort(prot.getSoilTemperatureLowerLimit());
        buf.writeShort(prot.getSoilHumidityUpperLimit());
        buf.writeShort(prot.getSoilHumidityLowerLimit());
        buf.writeShort(prot.getWindSpeedUpperLimit());
        buf.writeShort(prot.getWindSpeedLowerLimit());
        buf.writeShort(prot.getCo2DensityUpperLimit());
        buf.writeShort(prot.getCo2DensityLowerLimit());
        buf.writeShort(prot.getLightingUpperLimit());
        buf.writeShort(prot.getLightingLowerLimit());
        buf.writeShort(prot.getRainfallUpperLimit());
        buf.writeShort(prot.getRainfallLowerLimit());
        this.writeTime(buf,prot.getIncorrectTimeBegin());
        this.writeTime(buf,prot.getIncorrectTimeEnd());
        buf.writeShort(prot.getLockCmd());
        buf.writeInt(prot.getUnlockPassword());
        this.writeDateTime(buf,prot.getCreateTime());
        buf.writeByte(prot.getSubmitMode());

        this.writeTime(buf,prot.getBlanket1OpenTime());
        this.writeTime(buf,prot.getBlanket1CloseTime());
        this.writeTime(buf,prot.getFilm1OpenTime());
        this.writeTime(buf,prot.getFilm1CloseTime());
        this.writeTime(buf,prot.getBlanket2OpenTime());
        this.writeTime(buf,prot.getBlanket2CloseTime());
        this.writeTime(buf,prot.getFilm2OpenTime());
        this.writeTime(buf,prot.getFilm2CloseTime());
        this.writeTime(buf,prot.getValve1OpenTime());
        this.writeTime(buf,prot.getValve1CloseTime());
        this.writeTime(buf,prot.getValve2OpenTime());
        this.writeTime(buf,prot.getValve2CloseTime());
        this.writeTime(buf,prot.getShadingOpenTime());
        this.writeTime(buf,prot.getShadingCloseTime());
        this.writeTime(buf,prot.getHeatingOpenTime());
        this.writeTime(buf,prot.getHeatingCloseTime());
        this.writeTime(buf,prot.getLightingOpenTime());
        this.writeTime(buf,prot.getLightingCloseTime());
        this.writeTime(buf,prot.getWindOpenTime());
        this.writeTime(buf,prot.getWindCloseTime());
        this.writeTime(buf,prot.getCo2OpenTime());
        this.writeTime(buf,prot.getCo2CloseTime());
        this.writeTime(buf,prot.getResetTime());
        buf.writeByte(prot.getAutoStart());

        this.writeTime(buf,prot.getBlanket1OpenTime2());
        this.writeTime(buf,prot.getBlanket1CloseTime2());
        this.writeTime(buf,prot.getFilm1OpenTime2());
        this.writeTime(buf,prot.getFilm1CloseTime2());
        this.writeTime(buf,prot.getBlanket2OpenTime2());
        this.writeTime(buf,prot.getBlanket2CloseTime2());
        this.writeTime(buf,prot.getFilm2OpenTime2());
        this.writeTime(buf,prot.getFilm2CloseTime2());
        this.writeTime(buf,prot.getValve1OpenTime2());
        this.writeTime(buf,prot.getValve1CloseTime2());
        this.writeTime(buf,prot.getValve2OpenTime2());
        this.writeTime(buf,prot.getValve2CloseTime2());
        this.writeTime(buf,prot.getShadingOpenTime2());
        this.writeTime(buf,prot.getShadingCloseTime2());
        this.writeTime(buf,prot.getHeatingOpenTime2());
        this.writeTime(buf,prot.getHeatingCloseTime2());
        this.writeTime(buf,prot.getLightingOpenTime2());
        this.writeTime(buf,prot.getLightingCloseTime2());
        this.writeTime(buf,prot.getWindOpenTime2());
        this.writeTime(buf,prot.getWindCloseTime2());
        this.writeTime(buf,prot.getCo2OpenTime2());
        this.writeTime(buf,prot.getCo2CloseTime2());

        this.writeTime(buf,prot.getBlanket1OpenTime3());
        this.writeTime(buf,prot.getBlanket1CloseTime3());
        this.writeTime(buf,prot.getFilm1OpenTime3());
        this.writeTime(buf,prot.getFilm1CloseTime3());
        this.writeTime(buf,prot.getBlanket2OpenTime3());
        this.writeTime(buf,prot.getBlanket2CloseTime3());
        this.writeTime(buf,prot.getFilm2OpenTime3());
        this.writeTime(buf,prot.getFilm2CloseTime3());
        this.writeTime(buf,prot.getValve1OpenTime3());
        this.writeTime(buf,prot.getValve1CloseTime3());
        this.writeTime(buf,prot.getValve2OpenTime3());
        this.writeTime(buf,prot.getValve2CloseTime3());
        this.writeTime(buf,prot.getShadingOpenTime3());
        this.writeTime(buf,prot.getShadingCloseTime3());
        this.writeTime(buf,prot.getHeatingOpenTime3());
        this.writeTime(buf,prot.getHeatingCloseTime3());
        this.writeTime(buf,prot.getLightingOpenTime3());
        this.writeTime(buf,prot.getLightingCloseTime3());
        this.writeTime(buf,prot.getWindOpenTime3());
        this.writeTime(buf,prot.getWindCloseTime3());
        this.writeTime(buf,prot.getCo2OpenTime3());
        this.writeTime(buf,prot.getCo2CloseTime3());

        this.writeTime(buf,prot.getBlanket1OpenTime4());
        this.writeTime(buf,prot.getBlanket1CloseTime4());
        this.writeTime(buf,prot.getFilm1OpenTime4());
        this.writeTime(buf,prot.getFilm1CloseTime4());
        this.writeTime(buf,prot.getBlanket2OpenTime4());
        this.writeTime(buf,prot.getBlanket2CloseTime4());
        this.writeTime(buf,prot.getFilm2OpenTime4());
        this.writeTime(buf,prot.getFilm2CloseTime4());
        this.writeTime(buf,prot.getValve1OpenTime4());
        this.writeTime(buf,prot.getValve1CloseTime4());
        this.writeTime(buf,prot.getValve2OpenTime4());
        this.writeTime(buf,prot.getValve2CloseTime4());
        this.writeTime(buf,prot.getShadingOpenTime4());
        this.writeTime(buf,prot.getShadingCloseTime4());
        this.writeTime(buf,prot.getHeatingOpenTime4());
        this.writeTime(buf,prot.getHeatingCloseTime4());
        this.writeTime(buf,prot.getLightingOpenTime4());
        this.writeTime(buf,prot.getLightingCloseTime4());
        this.writeTime(buf,prot.getWindOpenTime4());
        this.writeTime(buf,prot.getWindCloseTime4());
        this.writeTime(buf,prot.getCo2OpenTime4());
        this.writeTime(buf,prot.getCo2CloseTime4());

        buf.writeByte(prot.getBlanket1Open());
        buf.writeByte(prot.getBlanket1Close());
        buf.writeByte(prot.getBlanket1Stop());
        buf.writeByte(prot.getFilm1Open());
        buf.writeByte(prot.getFilm1Close());
        buf.writeByte(prot.getFilm1Stop());
        buf.writeByte(prot.getFilm2Open());
        buf.writeByte(prot.getFilm2Close());
        buf.writeByte(prot.getFilm2Stop());
        buf.writeByte(prot.getBlanket2Open());
        buf.writeByte(prot.getBlanket2Close());
        buf.writeByte(prot.getBlanket2Stop());
        buf.writeByte(prot.getShadingOpen());
        buf.writeByte(prot.getShadingClose());
        buf.writeByte(prot.getShadingStop());
        buf.writeByte(prot.getHeatingOpen());
        buf.writeByte(prot.getHeatingClose());
        buf.writeByte(prot.getCo2Open());
        buf.writeByte(prot.getCo2Close());
        buf.writeByte(prot.getLightingOpen());
        buf.writeByte(prot.getLightingClose());
        buf.writeByte(prot.getWateringOpen());
        buf.writeByte(prot.getWateringClose());
        buf.writeByte(prot.getWindOpen());
        buf.writeByte(prot.getWindClose());

        //从buf中获取指定开始位置，指定长度的数据
        byte[] bytes = new byte[prot.getContextLength()];
        buf.getBytes(prot.getStartIndex(),bytes);
        buffer.writeBytes(bytes);
    }
*/

    private void writeTime(ByteBuf buf,LocalTime time){
        if(time != null) {
            buf.writeByte(time.getHour());
            buf.writeByte(time.getMinute());
        }else{
            buf.writeByte(0);
            buf.writeByte(0);
        }
    }
    private void writeDateTime(ByteBuf buf,LocalDateTime dateTime){
        if(dateTime != null) {
            buf.writeByte(dateTime.getYear());
            buf.writeByte(dateTime.getMonthValue());
            buf.writeByte(dateTime.getDayOfMonth());
            buf.writeByte(dateTime.getHour());
            buf.writeByte(dateTime.getMinute());
            buf.writeByte(dateTime.getSecond());
        }else{
            buf.writeByte(0);
            buf.writeByte(0);
            buf.writeByte(0);
            buf.writeByte(0);
            buf.writeByte(0);
            buf.writeByte(0);
        }
    }
}
