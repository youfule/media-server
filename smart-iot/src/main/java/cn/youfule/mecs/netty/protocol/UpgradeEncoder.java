package cn.youfule.mecs.netty.protocol;

import cn.youfule.mecs.netty.util.Constant;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;

public class UpgradeEncoder extends AbstractEncoder<UpgradeOutboundProtocolContext>{

    @Override
    protected byte[] supportFunctionCode() {
        return new byte[]{Constant.OUT_FC_UPGRADE};
    }

    @Override
    protected void encodeContext(ChannelHandlerContext channelHandlerContext, UpgradeOutboundProtocolContext prot, ByteBuf buffer) {
        buffer.writeByte(prot.getFrameNumber());
        buffer.writeByte(prot.getContextLength());
//        buffer.writeByte(prot.getContext());
    }
}
