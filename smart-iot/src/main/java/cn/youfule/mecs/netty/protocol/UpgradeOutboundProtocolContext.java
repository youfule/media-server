package cn.youfule.mecs.netty.protocol;

import lombok.Data;

@Data
public class UpgradeOutboundProtocolContext extends AbstractProtocolContext{

    /**
     * 帧号
     */
    private int frameNumber;

//    private int startIndex;
//    private int contextLength;

    //其他内容
}
