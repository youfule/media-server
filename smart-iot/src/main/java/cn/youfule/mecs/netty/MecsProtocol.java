package cn.youfule.mecs.netty;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@NoArgsConstructor
public class MecsProtocol<I> {

    /**
     * 默认数据包启动位 0x55 0x66
     */
    public final static short START = 21862;
    /**
     * 默认厂家 0x01
     */
    public final static byte DEFAULT_FACTORY = 1;
    /**
     * 默认类型 0x0E
     */
    public final static byte DEFAULT_TYPE = 0x0E;

    public int id;
    public int factory = DEFAULT_FACTORY;
    public int type = DEFAULT_TYPE;
    public int functionCode;
    public I context;
    public int crc;

    public MecsProtocol(int id, int functionCode) {
        this.id = id;
        this.functionCode = functionCode;
    }
}
