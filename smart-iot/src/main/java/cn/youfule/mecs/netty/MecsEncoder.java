package cn.youfule.mecs.netty;

import cn.youfule.mecs.netty.protocol.*;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

public class MecsEncoder extends MessageToByteEncoder<MecsProtocol> {

    private static List<AbstractEncoder> encoders;

    static {
        encoders = new ArrayList<>();
        encoders.add(new LoginEncoder());
        encoders.add(new LogoutEncoder());
        encoders.add(new QueryEncoder());
        encoders.add(new ReportEncoder());
        encoders.add(new UpgradeEncoder());
        encoders.add(new SettingEncoder());
        encoders.add(new HeartbeatEncoder());
        encoders.add(new SearchEncoder());
    }

    @Override
    protected void encode(ChannelHandlerContext ctx, MecsProtocol prot, ByteBuf buffer) throws Exception {
        for(AbstractEncoder encoder:encoders){
            if(encoder.isSupport(prot)){
                encoder.encode(ctx,prot,buffer);
            }
        }
    }
}
