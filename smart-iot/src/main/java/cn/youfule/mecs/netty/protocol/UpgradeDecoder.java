package cn.youfule.mecs.netty.protocol;

import cn.youfule.mecs.netty.MecsProtocol;
import cn.youfule.mecs.netty.util.Constant;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;

public class UpgradeDecoder extends AbstractDecoder<UpgradeInboundProtocolContext>{

    @Override
    protected byte[] supportFunctionCode() {
        return new byte[]{Constant.IN_FC_UPGRADE};
    }

    @Override
    protected UpgradeInboundProtocolContext decodeContext(ChannelHandlerContext channelHandlerContext, MecsProtocol<UpgradeInboundProtocolContext> prot, ByteBuf buffer) {
        UpgradeInboundProtocolContext context = new UpgradeInboundProtocolContext();
        context.setFrameNumber(buffer.readByte());
        context.setContextLength(buffer.readByte());
        return context;
    }

    @Override
    protected int supportMinLength() {
        return Constant.UPGRADE_BUF_MIN_LEN;
    }
}
