package cn.youfule.mecs.netty.protocol;

import cn.youfule.mecs.netty.util.Constant;

public class LoginDecoder extends AbstractDecoder<LoginInboundProtocolContext>{

    @Override
    protected byte[] supportFunctionCode() {
        return new byte[]{Constant.IN_FC_LOGIN};
    }

/*    @Override
    protected LoginInboundProtocolContext decodeContext(ChannelHandlerContext channelHandlerContext, MecsProtocol<LoginInboundProtocolContext> prot, ByteBuf buffer) {
        LoginInboundProtocolContext context = new LoginInboundProtocolContext();
        context.setStartIndex(buffer.readByte());
        context.setContextLength(buffer.readByte());
        return context;
    }*/

    @Override
    protected int supportMinLength() {
        return Constant.LOGIN_BUF_MIN_LEN;
    }
}
