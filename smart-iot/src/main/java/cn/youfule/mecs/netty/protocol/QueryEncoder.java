package cn.youfule.mecs.netty.protocol;

import cn.youfule.mecs.netty.util.Constant;

public class QueryEncoder extends AbstractEncoder<QueryOutboundProtocolContext>{

    @Override
    protected byte[] supportFunctionCode() {
        return new byte[]{Constant.OUT_FC_QUERY};
    }

/*    @Override
    protected void encodeContext(ChannelHandlerContext channelHandlerContext, QueryOutboundProtocolContext prot, ByteBuf buffer) {
        buffer.writeByte(prot.getStartIndex());
        buffer.writeByte(prot.getContextLength());
    }*/
}
