package cn.youfule.mecs.netty.protocol;

import cn.youfule.mecs.netty.util.Constant;

public class SearchEncoder extends AbstractEncoder<SearchOutboundProtocolContext>{

    @Override
    protected byte[] supportFunctionCode() {
        return new byte[]{Constant.OUT_FC_SEARCH};
    }

/*    @Override
    protected void encodeContext(ChannelHandlerContext channelHandlerContext, SearchOutboundProtocolContext prot, ByteBuf buffer) {
        buffer.writeByte(prot.getStartIndex());
        buffer.writeByte(prot.getContextLength());
    }*/
}
