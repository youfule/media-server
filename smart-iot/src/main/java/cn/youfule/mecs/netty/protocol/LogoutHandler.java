package cn.youfule.mecs.netty.protocol;

import cn.youfule.mecs.netty.MecsProtocol;
import cn.youfule.mecs.netty.util.Constant;
import io.netty.channel.ChannelHandlerContext;

public class LogoutHandler extends AbstractHandler<LogoutInboundProtocolContext, LogoutOutboundProtocolContext>{
    @Override
    protected byte[] supportFunctionCode() {
        return new byte[]{Constant.IN_FC_LOGOUT};
    }

    @Override
    protected LogoutOutboundProtocolContext outboundContextBuild(ChannelHandlerContext ctx, MecsProtocol<LogoutInboundProtocolContext> inbound, MecsProtocol<LogoutOutboundProtocolContext> outbound) throws Exception {
        LogoutOutboundProtocolContext context = new LogoutOutboundProtocolContext();
        context.setStartIndex(0);
        context.setContextLength(0);
        context.setContext(0);
        return context;
    }
}
