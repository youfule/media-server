package cn.youfule.mecs.netty.protocol;

import cn.youfule.mecs.netty.MecsProtocol;
import cn.youfule.mecs.netty.util.Constant;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;

public class SettingDecoder extends AbstractDecoder<SettingInboundProtocolContext>{

    @Override
    protected byte[] supportFunctionCode() {
        return new byte[]{Constant.IN_FC_SETTING};
    }

    @Override
    protected SettingInboundProtocolContext decodeContext(ChannelHandlerContext channelHandlerContext, MecsProtocol<SettingInboundProtocolContext> prot, ByteBuf buffer) {
        SettingInboundProtocolContext context = new SettingInboundProtocolContext();
        context.setStartIndex(buffer.readByte());
        context.setContextLength(buffer.readByte());
        return context;
    }

    @Override
    protected int supportMinLength() {
        return Constant.SETTING_BUF_MIN_LEN;
    }
}
