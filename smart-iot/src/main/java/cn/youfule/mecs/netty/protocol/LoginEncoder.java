package cn.youfule.mecs.netty.protocol;

import cn.youfule.mecs.netty.util.Constant;

public class LoginEncoder extends AbstractEncoder<LoginOutboundProtocolContext>{

    @Override
    protected byte[] supportFunctionCode() {
        return new byte[]{Constant.OUT_FC_LOGIN};
    }

/*    @Override
    protected void encodeContext(ChannelHandlerContext channelHandlerContext, LoginOutboundProtocolContext prot, ByteBuf buffer) {
        buffer.writeByte(prot.getStartIndex());
        buffer.writeByte(prot.getContextLength());
        buffer.writeByte(prot.getContext());
    }*/
}
