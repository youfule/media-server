package cn.youfule.mecs.netty;

import java.lang.annotation.*;

/**
 * @author fortuneju
 */
//作用域Javadoc
@Documented
//可继承
@Inherited
/*
描述了注解修饰的对象范围，取值在java.lang.annotation.ElementType定义，常用的包括：
METHOD：用于描述方法
PACKAGE：用于描述包
PARAMETER：用于描述方法变量
TYPE：用于描述类、接口或enum类型
*/
@Target(ElementType.FIELD)
/*
表示注解保留时间长短。取值在java.lang.annotation.RetentionPolicy中，取值为：
SOURCE：在源文件中有效，编译过程中会被忽略
CLASS：随源文件一起编译在class文件中，运行时忽略
RUNTIME：在运行时有效
*/
@Retention(RetentionPolicy.RUNTIME)
public @interface ProtocolField {

//    int index();
    int start();
    int length() default 1;
}
