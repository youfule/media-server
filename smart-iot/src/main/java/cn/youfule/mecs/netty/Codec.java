package cn.youfule.mecs.netty;

import java.lang.annotation.*;

@Documented
@Inherited
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface Codec {
    int functionCode() ;
    int minLength();
}
