package cn.youfule.mecs.netty.protocol;


import lombok.Data;

@Data
public class UpgradeInboundProtocolContext extends AbstractProtocolContext {

    /**
     * 帧号
     */
    private int frameNumber;

//    private int contextLength;
}
