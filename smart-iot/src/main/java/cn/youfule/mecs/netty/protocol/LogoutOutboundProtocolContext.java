package cn.youfule.mecs.netty.protocol;

import cn.youfule.mecs.netty.ProtocolField;
import lombok.Data;

@Data
public class LogoutOutboundProtocolContext extends AbstractProtocolContext{
/*    private int startIndex;
    private int contextLength;*/
    //0x00
    @ProtocolField(start = 0,length = 1)
    private int context = 0;

}
