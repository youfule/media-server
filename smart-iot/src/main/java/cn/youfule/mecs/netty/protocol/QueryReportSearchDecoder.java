package cn.youfule.mecs.netty.protocol;

import cn.youfule.mecs.netty.util.Constant;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class QueryReportSearchDecoder extends AbstractDecoder<QueryReportSearchInboundProtocolContext>{


    @Override
    protected byte[] supportFunctionCode() {
        return new byte[]{
                Constant.IN_FC_QUERY,Constant.IN_FC_REPORT,Constant.IN_FC_SEARCH
        };
    }

    @Override
    protected int supportMinLength() {
        return Constant.QUERY_REPORT_SEARCH_BUF_MIN_LEN;
    }

/*    @Override
    protected QueryReportSearchInboundProtocolContext decodeContext(ChannelHandlerContext channelHandlerContext, MecsProtocol<QueryReportSearchInboundProtocolContext> prot, ByteBuf buffer) {
        QueryReportSearchInboundProtocolContext context = new QueryReportSearchInboundProtocolContext();
        context.setStartIndex(buffer.readByte());
        context.setContextLength(buffer.readByte());

        // 其他数据
        context.setRunState(buffer.readByte());
        context.setFault(buffer.readInt());
        context.setAirTemperature(buffer.readShort());
        context.setAirHumidity(buffer.readShort());
        context.setSoilTemperature(buffer.readShort());
        context.setSoilHumidity(buffer.readShort());
        context.setWindSpeed(buffer.readShort());
        context.setCo2Density(buffer.readShort());
        context.setLighting(buffer.readShort());
        context.setRainfall(buffer.readShort());
        context.setIncorrectTimeBegin(LocalTime.of(buffer.readByte(),buffer.readByte()));
        context.setIncorrectTimeEnd(LocalTime.of(buffer.readByte(),buffer.readByte()));
        context.setSensorNumber(buffer.readByte());
        context.setAirTemperatureUpperLimit(buffer.readShort());
        context.setAirTemperatureLowerLimit(buffer.readShort());
        context.setAirHumidityUpperLimit(buffer.readShort());
        context.setAirHumidityLowerLimit(buffer.readShort());
        context.setSoilTemperatureUpperLimit(buffer.readShort());
        context.setSoilTemperatureLowerLimit(buffer.readShort());
        context.setSoilHumidityUpperLimit(buffer.readShort());
        context.setSoilHumidityLowerLimit(buffer.readShort());
        context.setWindSpeedUpperLimit(buffer.readShort());
        context.setWindSpeedLowerLimit(buffer.readShort());
        context.setCo2DensityUpperLimit(buffer.readShort());
        context.setCo2DensityLowerLimit(buffer.readShort());
        context.setLightingUpperLimit(buffer.readShort());
        context.setLightingLowerLimit(buffer.readShort());
        context.setRainfallUpperLimit(buffer.readShort());
        context.setRainfallLowerLimit(buffer.readShort());
        context.setCreateTime(LocalDateTime.of(
                buffer.readByte(),buffer.readByte(),buffer.readByte(),
                buffer.readByte(),buffer.readByte(),buffer.readByte()
        ));
        context.setSubmitMode(buffer.readByte());

        context.setBlanket1OpenTime(LocalTime.of(buffer.readByte(),buffer.readByte()));
        context.setBlanket1CloseTime(LocalTime.of(buffer.readByte(),buffer.readByte()));
        context.setFilm1OpenTime(LocalTime.of(buffer.readByte(),buffer.readByte()));
        context.setFilm1CloseTime(LocalTime.of(buffer.readByte(),buffer.readByte()));
        context.setBlanket2OpenTime(LocalTime.of(buffer.readByte(),buffer.readByte()));
        context.setBlanket2CloseTime(LocalTime.of(buffer.readByte(),buffer.readByte()));
        context.setFilm2OpenTime(LocalTime.of(buffer.readByte(),buffer.readByte()));
        context.setFilm2CloseTime(LocalTime.of(buffer.readByte(),buffer.readByte()));
        context.setValve1OpenTime(LocalTime.of(buffer.readByte(),buffer.readByte()));
        context.setValve1CloseTime(LocalTime.of(buffer.readByte(),buffer.readByte()));
        context.setValve2OpenTime(LocalTime.of(buffer.readByte(),buffer.readByte()));
        context.setValve2CloseTime(LocalTime.of(buffer.readByte(),buffer.readByte()));
        context.setShadingOpenTime(LocalTime.of(buffer.readByte(),buffer.readByte()));
        context.setShadingCloseTime(LocalTime.of(buffer.readByte(),buffer.readByte()));
        context.setHeatingOpenTime(LocalTime.of(buffer.readByte(),buffer.readByte()));
        context.setHeatingCloseTime(LocalTime.of(buffer.readByte(),buffer.readByte()));
        context.setLightingOpenTime(LocalTime.of(buffer.readByte(),buffer.readByte()));
        context.setLightingCloseTime(LocalTime.of(buffer.readByte(),buffer.readByte()));
        context.setWindOpenTime(LocalTime.of(buffer.readByte(),buffer.readByte()));
        context.setWindCloseTime(LocalTime.of(buffer.readByte(),buffer.readByte()));
        context.setCo2OpenTime(LocalTime.of(buffer.readByte(),buffer.readByte()));
        context.setCo2CloseTime(LocalTime.of(buffer.readByte(),buffer.readByte()));
        context.setResetTime(LocalTime.of(buffer.readByte(),buffer.readByte()));
        context.setAutoStart(buffer.readByte());

        context.setBlanket1OpenState(buffer.readByte());
        context.setBlanket1CloseState(buffer.readByte());
        context.setFilm1OpenState(buffer.readByte());
        context.setFilm1CloseState(buffer.readByte());
        context.setBlanket2OpenState(buffer.readByte());
        context.setBlanket2CloseState(buffer.readByte());
        context.setFilm2OpenState(buffer.readByte());
        context.setFilm2CloseState(buffer.readByte());
        context.setShadingOpenState(buffer.readByte());
        context.setShadingCloseState(buffer.readByte());
        context.setHeatingOpenState(buffer.readByte());
        context.setLightingOpenState(buffer.readByte());
        context.setWateringState(buffer.readByte());
        context.setCo2OpenState(buffer.readByte());
        context.setWindOpenState(buffer.readByte());

        context.setBlanket1OpenTime2(LocalTime.of(buffer.readByte(),buffer.readByte()));
        context.setBlanket1CloseTime2(LocalTime.of(buffer.readByte(),buffer.readByte()));
        context.setFilm1OpenTime2(LocalTime.of(buffer.readByte(),buffer.readByte()));
        context.setFilm1CloseTime2(LocalTime.of(buffer.readByte(),buffer.readByte()));
        context.setBlanket2OpenTime2(LocalTime.of(buffer.readByte(),buffer.readByte()));
        context.setBlanket2CloseTime2(LocalTime.of(buffer.readByte(),buffer.readByte()));
        context.setFilm2OpenTime2(LocalTime.of(buffer.readByte(),buffer.readByte()));
        context.setFilm2CloseTime2(LocalTime.of(buffer.readByte(),buffer.readByte()));
        context.setValve1OpenTime2(LocalTime.of(buffer.readByte(),buffer.readByte()));
        context.setValve1CloseTime2(LocalTime.of(buffer.readByte(),buffer.readByte()));
        context.setValve2OpenTime2(LocalTime.of(buffer.readByte(),buffer.readByte()));
        context.setValve2CloseTime2(LocalTime.of(buffer.readByte(),buffer.readByte()));
        context.setShadingOpenTime2(LocalTime.of(buffer.readByte(),buffer.readByte()));
        context.setShadingCloseTime2(LocalTime.of(buffer.readByte(),buffer.readByte()));
        context.setHeatingOpenTime2(LocalTime.of(buffer.readByte(),buffer.readByte()));
        context.setHeatingCloseTime2(LocalTime.of(buffer.readByte(),buffer.readByte()));
        context.setLightingOpenTime2(LocalTime.of(buffer.readByte(),buffer.readByte()));
        context.setLightingCloseTime2(LocalTime.of(buffer.readByte(),buffer.readByte()));
        context.setWindOpenTime2(LocalTime.of(buffer.readByte(),buffer.readByte()));
        context.setWindCloseTime2(LocalTime.of(buffer.readByte(),buffer.readByte()));
        context.setCo2OpenTime2(LocalTime.of(buffer.readByte(),buffer.readByte()));
        context.setCo2CloseTime2(LocalTime.of(buffer.readByte(),buffer.readByte()));

        context.setBlanket1OpenTime3(LocalTime.of(buffer.readByte(),buffer.readByte()));
        context.setBlanket1CloseTime3(LocalTime.of(buffer.readByte(),buffer.readByte()));
        context.setFilm1OpenTime3(LocalTime.of(buffer.readByte(),buffer.readByte()));
        context.setFilm1CloseTime3(LocalTime.of(buffer.readByte(),buffer.readByte()));
        context.setBlanket2OpenTime3(LocalTime.of(buffer.readByte(),buffer.readByte()));
        context.setBlanket2CloseTime3(LocalTime.of(buffer.readByte(),buffer.readByte()));
        context.setFilm2OpenTime3(LocalTime.of(buffer.readByte(),buffer.readByte()));
        context.setFilm2CloseTime3(LocalTime.of(buffer.readByte(),buffer.readByte()));
        context.setValve1OpenTime3(LocalTime.of(buffer.readByte(),buffer.readByte()));
        context.setValve1CloseTime3(LocalTime.of(buffer.readByte(),buffer.readByte()));
        context.setValve2OpenTime3(LocalTime.of(buffer.readByte(),buffer.readByte()));
        context.setValve2CloseTime3(LocalTime.of(buffer.readByte(),buffer.readByte()));
        context.setShadingOpenTime3(LocalTime.of(buffer.readByte(),buffer.readByte()));
        context.setShadingCloseTime3(LocalTime.of(buffer.readByte(),buffer.readByte()));
        context.setHeatingOpenTime3(LocalTime.of(buffer.readByte(),buffer.readByte()));
        context.setHeatingCloseTime3(LocalTime.of(buffer.readByte(),buffer.readByte()));
        context.setLightingOpenTime3(LocalTime.of(buffer.readByte(),buffer.readByte()));
        context.setLightingCloseTime3(LocalTime.of(buffer.readByte(),buffer.readByte()));
        context.setWindOpenTime3(LocalTime.of(buffer.readByte(),buffer.readByte()));
        context.setWindCloseTime3(LocalTime.of(buffer.readByte(),buffer.readByte()));
        context.setCo2OpenTime3(LocalTime.of(buffer.readByte(),buffer.readByte()));
        context.setCo2CloseTime3(LocalTime.of(buffer.readByte(),buffer.readByte()));

        context.setBlanket1OpenTime4(LocalTime.of(buffer.readByte(),buffer.readByte()));
        context.setBlanket1CloseTime4(LocalTime.of(buffer.readByte(),buffer.readByte()));
        context.setFilm1OpenTime4(LocalTime.of(buffer.readByte(),buffer.readByte()));
        context.setFilm1CloseTime4(LocalTime.of(buffer.readByte(),buffer.readByte()));
        context.setBlanket2OpenTime4(LocalTime.of(buffer.readByte(),buffer.readByte()));
        context.setBlanket2CloseTime4(LocalTime.of(buffer.readByte(),buffer.readByte()));
        context.setFilm2OpenTime4(LocalTime.of(buffer.readByte(),buffer.readByte()));
        context.setFilm2CloseTime4(LocalTime.of(buffer.readByte(),buffer.readByte()));
        context.setValve1OpenTime4(LocalTime.of(buffer.readByte(),buffer.readByte()));
        context.setValve1CloseTime4(LocalTime.of(buffer.readByte(),buffer.readByte()));
        context.setValve2OpenTime4(LocalTime.of(buffer.readByte(),buffer.readByte()));
        context.setValve2CloseTime4(LocalTime.of(buffer.readByte(),buffer.readByte()));
        context.setShadingOpenTime4(LocalTime.of(buffer.readByte(),buffer.readByte()));
        context.setShadingCloseTime4(LocalTime.of(buffer.readByte(),buffer.readByte()));
        context.setHeatingOpenTime4(LocalTime.of(buffer.readByte(),buffer.readByte()));
        context.setHeatingCloseTime4(LocalTime.of(buffer.readByte(),buffer.readByte()));
        context.setLightingOpenTime4(LocalTime.of(buffer.readByte(),buffer.readByte()));
        context.setLightingCloseTime4(LocalTime.of(buffer.readByte(),buffer.readByte()));
        context.setWindOpenTime4(LocalTime.of(buffer.readByte(),buffer.readByte()));
        context.setWindCloseTime4(LocalTime.of(buffer.readByte(),buffer.readByte()));
        context.setCo2OpenTime4(LocalTime.of(buffer.readByte(),buffer.readByte()));
        context.setCo2CloseTime4(LocalTime.of(buffer.readByte(),buffer.readByte()));
        return context;
    }*/

}
