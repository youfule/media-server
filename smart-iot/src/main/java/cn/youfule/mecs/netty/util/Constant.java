package cn.youfule.mecs.netty.util;

public interface Constant {

    /**
     * 登录入站
     */
    byte IN_FC_LOGIN = 0x01;
    /**
     * 登录出站
     */
    byte OUT_FC_LOGIN = 0x01;

    /**
     * 退出入站
     */
    byte IN_FC_LOGOUT = 0x02;
    /**
     * 退出出站
     */
    byte OUT_FC_LOGOUT = 0x02;

    /**
     * 查询上报入站
     */
    byte IN_FC_QUERY = 0x03;
    /**
     * 查询上报出站
     */
    byte OUT_FC_QUERY = 0x03;

    /**
     * 从设备主动上报入站
     */
    byte IN_FC_REPORT = 0x08;
    /**
     * 从设备主动上报出站
     */
    byte OUT_FC_REPORT = 0x08;

    /**
     * 设备固件升级入站
     */
    byte IN_FC_UPGRADE = 0x09;
    /**
     * 设备固件升级出站
     */
    byte OUT_FC_UPGRADE = 0x09;

    /**
     * 参数下发入站
     */
    byte IN_FC_SETTING = 0x10;
    /**
     * 参数下发出站
     */
    byte OUT_FC_SETTING = 0x10;

    /**
     * 心跳命令入站
     */
    byte IN_FC_HEARTBEAT = 0x18;
    /**
     * 心跳命令出站
     */
    byte OUT_FC_HEARTBEAT = 0x19;

    /**
     * 查询上报入站
     */
    byte IN_FC_SEARCH = 0x20;
    /**
     * 查询上报出站
     */
    byte OUT_FC_SEARCH = 0x20;

    /**
     * 从设备心跳上传最小数据长度
     */
    int HEARTBEAT_BUF_MIN_LEN = 11;
    /**
     * 从设备登录上传最小数据长度
     */
    int LOGIN_BUF_MIN_LEN = 13;
    /**
     * 从设备登出上传最小数据长度
     */
    int LOGOUT_BUF_MIN_LEN = 13;
    /**
     * 从设备查询应答、主动上报上传最小数据长度
     */
    int QUERY_REPORT_SEARCH_BUF_MIN_LEN = 13;
    /**
     * 从设备参数设置应答上传最小数据长度
     */
    int SETTING_BUF_MIN_LEN = 13;
    /**
     * 从设备升级应答上传最小数据长度
     */
    int UPGRADE_BUF_MIN_LEN = 14;


}
