package cn.youfule.mecs.netty;

import cn.youfule.mecs.netty.protocol.*;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

public class MecsDecoder extends ByteToMessageDecoder {

    private static List<AbstractDecoder> decoders;

    static {
        decoders = new ArrayList<>();
        decoders.add(new LoginDecoder());
        decoders.add(new LogoutDecoder());
        decoders.add(new QueryReportSearchDecoder());
        decoders.add(new UpgradeDecoder());
        decoders.add(new SettingDecoder());
        decoders.add(new HeartbeatDecoder());
    }

    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf buffer, List<Object> list) throws Exception {
        for(AbstractDecoder decoder:decoders){
            if(decoder.isSupport(buffer)){
                decoder.decode(ctx,buffer,list);
            }
        }
    }
}
