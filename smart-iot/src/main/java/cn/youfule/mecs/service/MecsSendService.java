package cn.youfule.mecs.service;

import cn.youfule.mecs.netty.ChannelSupervise;
import cn.youfule.mecs.netty.MecsProtocol;
import cn.youfule.mecs.netty.protocol.QueryOutboundProtocolContext;
import cn.youfule.mecs.netty.protocol.QueryReportSearchInboundProtocolContext;
import cn.youfule.mecs.netty.protocol.SettingInboundProtocolContext;
import cn.youfule.mecs.netty.protocol.SettingOutboundProtocolContext;
import cn.youfule.mecs.netty.util.Constant;
import org.springframework.stereotype.Service;

import java.time.LocalTime;

@Service
public class MecsSendService {

/*    public void send(String ip, int deviceNum, byte startIndex, byte contextLength, AbstractProtocol prot){
        prot.setId(deviceNum)
            .setFunctionCode(MecsProtocol.FC_SETTING_SEND)
            .setStartIndex(startIndex)
            .setContextLength(contextLength)
            .setContext(context);
        NettyServer.sendMsg(ip,prot);
    }*/


    public MecsProtocol<SettingInboundProtocolContext> operationSend(String ip, int deviceNum, Integer code){
        MecsProtocol<SettingOutboundProtocolContext> prot = new MecsProtocol<>(deviceNum,Constant.OUT_FC_SETTING);
        SettingOutboundProtocolContext context = new SettingOutboundProtocolContext();
        //从索引为228+code开始，code为1-25
        context.setStartIndex(228+code)
                .setContextLength(1);
        context.setBlanket1Open(code == 1 ? code:0);
        context.setBlanket1Close(code == 2 ? code:0);
        context.setBlanket1Stop(code == 3 ? code:0);
        context.setFilm1Open(code == 4 ? code:0);
        context.setFilm1Close(code == 5 ? code:0);
        context.setFilm1Stop(code == 6 ? code:0);
        context.setFilm2Open(code == 7 ? code:0);
        context.setFilm2Close(code == 8 ? code:0);
        context.setFilm2Stop(code == 9 ? code:0);
        context.setBlanket2Open(code == 10 ? code:0);
        context.setBlanket2Close(code == 11 ? code:0);
        context.setBlanket2Stop(code == 12 ? code:0);
        context.setShadingOpen(code == 13 ? code:0);
        context.setShadingClose(code == 14 ? code:0);
        context.setShadingStop(code == 15 ? code:0);
        context.setHeatingOpen(code == 16 ? code:0);
        context.setHeatingClose(code == 17 ? code:0);
        context.setCo2Open(code == 18 ? code:0);
        context.setCo2Close(code == 19 ? code:0);
        context.setLightingOpen(code == 20 ? code:0);
        context.setLightingClose(code == 21 ? code:0);
        context.setWateringOpen(code == 22 ? code:0);
        context.setWateringClose(code == 23 ? code:0);
        context.setWindOpen(code == 24 ? code:0);
        context.setWindClose(code == 25 ? code:0);
        prot.setContext(context);
        return ChannelSupervise.send2Single(ip,prot);
    }

    public MecsProtocol<SettingInboundProtocolContext> incorrectTimeSend(String ip, int deviceNum, LocalTime incorrectTimeBegin, LocalTime incorrectTimeEnd){
        MecsProtocol<SettingOutboundProtocolContext> prot = new MecsProtocol<>(deviceNum,Constant.OUT_FC_SETTING);
        SettingOutboundProtocolContext context = new SettingOutboundProtocolContext();
        context.setStartIndex(34)
                .setContextLength(4);
        context.setIncorrectTimeBegin(incorrectTimeBegin)
                .setIncorrectTimeEnd(incorrectTimeEnd);
        prot.setContext(context);
        return ChannelSupervise.send2Single(ip,prot);
    }

    public MecsProtocol<QueryReportSearchInboundProtocolContext> querySend(String ip, int deviceNum, int startIndex, int contextLength) {
        MecsProtocol<QueryOutboundProtocolContext> prot = new MecsProtocol<>(deviceNum,Constant.OUT_FC_QUERY);
        QueryOutboundProtocolContext context = new QueryOutboundProtocolContext();
        context.setStartIndex(startIndex)
                .setContextLength(contextLength);
        prot.setContext(context);
        return ChannelSupervise.send2Single(ip,prot);
    }
}
