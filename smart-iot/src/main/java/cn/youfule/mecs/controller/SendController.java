package cn.youfule.mecs.controller;

import cn.youfule.mecs.netty.MecsProtocol;
import cn.youfule.mecs.netty.protocol.QueryReportSearchInboundProtocolContext;
import cn.youfule.mecs.netty.protocol.SettingInboundProtocolContext;
import cn.youfule.mecs.service.MecsSendService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;
import java.time.LocalTime;

@RestController
@RequestMapping(value = "/mecs/send")
public class SendController {

    @Autowired
    private MecsSendService sendService;

    @GetMapping("operation")
    public MecsProtocol<SettingInboundProtocolContext> operationSend(@NotNull Integer code){
        return sendService.operationSend("192.168.2.6",9,code);
    }

    @GetMapping("incorrectTime")
    public MecsProtocol<SettingInboundProtocolContext> incorrectTimeSend(@NotNull Integer code){
        return sendService.incorrectTimeSend("192.168.2.26",9, LocalTime.now(),LocalTime.now());
    }
    @GetMapping("query")
    public MecsProtocol<QueryReportSearchInboundProtocolContext> querySend(){
        return sendService.querySend("192.168.2.6",9, 0,1);
    }

}
