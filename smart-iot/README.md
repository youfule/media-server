cn.youfule.iot包为网络demo案例

参考文档  
[蚂蚁通讯框架SOFABolt之私有通讯协议设计](https://cloud.tencent.com/developer/article/1504649)  
[物联网架构成长之路(35)-利用Netty解析物联网自定义协议(示例代码)](https://www.136.la/net/show-50925.html)  
[java与固件通信解析字节流报文](https://bbs.csdn.net/topics/396739297?list=lz)  
[netty请求响应的拟同步处理](https://m.imooc.com/article/details?article_id=260010)  

    该文档实现的主要功能为：
    通过websocket与前端建立连接，获取消息中的token作为key，将websocket channel进行缓存(参考TextWebSocketFrameHandler.java)；
    web请求发送消息，传递socket channelID、msg、token，服务器向设备发送消息，并且将token,和msg中的tip构造为dto，以channelId为key将dto保存到缓存中(参考NCBackController.java)；
    当服务器接收到设备返回的响应信息时，解析响应信息中的tip，通过channelId获取之前缓存的dto，通过dto中的token获取缓存中的websocket channel向用户发送响应消息(TCPServerHandler.java)；

[使用netty自带连接池，异步发送消息，同步获取返回结果（利用BlockingQueue回调）](https://gitee.com/xjmroot/netty-pool)  
[使用CountDownLatch实现netty发送消息后同步获取响应结果](https://blog.csdn.net/u010905359/article/details/90107713)
[基于netty的物联网通讯协议服务端实现](https://gitee.com/lincolnking/protocol-sdk)
[Netty实战系列一之多协议并存](https://blog.csdn.net/wangwei19871103/article/details/104840540)
[Netty多协议开发](https://blog.csdn.net/weixin_34232617/article/details/93882406)
    
