package cn.youfule.rtsp2ws;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Rtsp2wsApplication {

    public static void main(String[] args) {
        SpringApplication.run(Rtsp2wsApplication.class, args);
    }

}
