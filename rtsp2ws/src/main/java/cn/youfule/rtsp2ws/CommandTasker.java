package cn.youfule.rtsp2ws;

import lombok.Data;
import lombok.ToString;

/**
 * 用于存放任务id,任务主进程，任务输出线程
 *
 * @author
 * @since
 * @version
 */
@Data
@ToString
public class CommandTasker {
    private final String id;// 任务id
    private final String command;//命令行
    private Process process;// 命令行运行主进程
    private DealProcessStream inputThread;// 命令行消息输出子线程
    private DealProcessStream errorThread;// 命令行错误消息输出子线程

    public CommandTasker(String id,String command) {
        this.id = id;
        this.command=command;
    }

    public CommandTasker(String id,String command, Process process, DealProcessStream inputThread, DealProcessStream errorThread) {
        this.id = id;
        this.command=command;
        this.process = process;
        this.inputThread = inputThread;
        this.errorThread = errorThread;
    }

}