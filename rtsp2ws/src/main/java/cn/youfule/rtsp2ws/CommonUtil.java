package cn.youfule.rtsp2ws;

import cn.hutool.core.util.HashUtil;
import cn.hutool.core.util.StrUtil;

public class CommonUtil {

    /**
     * 视频源参数名称
     */
    public static final String PARAM_SOURCE = "source";
    /**
     * 视频大小参数名称
     */
    public static final String PARAM_RS = "rs";
    /**
     * 默认视频大小
     */
    public static final String PARAM_RS_DEFAULT = "800X600";


    /**
     * 获取source缓存key
     * @param source
     * @param rs
     * @return
     */
    public static String getSourceKey(String source,String rs){
        return "" + HashUtil.javaDefaultHash(source + rs);
    }
}
