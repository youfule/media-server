package cn.youfule.rtsp2ws;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@Controller
@RequestMapping("/rtsp")
public class RtspController {

    @Autowired
    private WsHandler wsHandler;

    @RequestMapping("/receive")
    @ResponseBody
    public String receive(HttpServletRequest request) {
        ServletInputStream inputStream = null;
        try {
            String sourceKey = request.getParameter("sourceKey");
            inputStream = request.getInputStream();
            int len = -1;
            while ((len =inputStream.available()) !=-1) {
                byte[] data = new byte[len];
                inputStream.read(data);
                wsHandler.sendVideo(sourceKey,data);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            try {
                inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        System.out.println("over");
        return "1";
    }



}
