package cn.youfule.rtsp2ws;


import lombok.extern.slf4j.Slf4j;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author
 * @version
 */
@Slf4j
public class ConvertVideoProcessManager {
    /**
     * 存放所有的推流进程
     */
    private static ConcurrentHashMap<String, CommandTasker> PROCESS_POOL = new ConcurrentHashMap<>();

    /**
     * 添加 process
     *
     * @param key
     */
    public static void add(String key, CommandTasker tasker) {
        PROCESS_POOL.put(key,tasker);
    }

    /**
     * 删除 process,会返回删除的 process
     *
     * @param key
     * @return
     */
    public static CommandTasker remove(String key) {
        return PROCESS_POOL.remove(key);
    }

    /**
     * 删除并同步关闭process
     *
     * @param key
     */
    public static void removeAndClose(String key) {
        CommandTasker tasker = remove(key);
        if (tasker != null) {
            // 销毁
            tasker.getInputThread().stop();
            tasker.getErrorThread().stop();
            tasker.getProcess().destroy();
        }
    }

    /**
     * 获得 process
     *
     * @param key
     * @return
     */
    public static CommandTasker get(String key) {
        return PROCESS_POOL.get(key);
    }

}