package cn.youfule.rtsp2ws;

import cn.hutool.core.util.StrUtil;
import org.springframework.stereotype.Component;

@Component
public class ConvertVideoPakcet {

    public Integer startPush(String source,String rs){
        int flag = -1;

        // ffmpeg位置，最好写在配置文件中
        String ffmpegPath = "M:\\GreenSoft\\ffmpeg\\bin\\";
        try {
            String sourceKey = CommonUtil.getSourceKey(source,rs);
            CommandTasker tasker ;
            synchronized(this) {
                // 获取正在处理指定IP视频流的进程
                tasker = ConvertVideoProcessManager.get(sourceKey);
                if (tasker != null) {
                    return 1;
                }
                // cmd命令拼接，注意命令中存在空格
                StringBuilder sb = new StringBuilder()
                        // ffmpeg位置
                        .append(ffmpegPath)
                        // ffmpeg开头，-re代表按照帧率发送，在推流时必须有
                        .append("ffmpeg ")
                        // 指定要推送的视频
                        .append(" -i \"").append(source).append("\"")
                        // 指定推送服务器，-f：指定格式
                        .append(" -q 0 -f mpegts -codec:v mpeg1video ");
                //视频大小参数
                if(StrUtil.isNotBlank(rs)){
                    sb.append(" -s ").append(rs).append(" ");
                }
                //视频流输出地址
                sb.append(this.outputStreamPath(sourceKey));

                String command = sb.toString();
                // 运行cmd命令，获取其进程
                Process process = Runtime.getRuntime().exec(command);
                //新启两个线程,输出ffmpeg推流日志
                DealProcessStream inputDeal = new DealProcessStream(process.getInputStream());
                inputDeal.start();
                DealProcessStream errorDeal = new DealProcessStream(process.getErrorStream());
                errorDeal.start();
                // 保存process
                tasker = new CommandTasker(sourceKey, command, process, inputDeal, errorDeal);
                ConvertVideoProcessManager.add(sourceKey, tasker);
            }
            flag = tasker.getProcess().waitFor();
        }catch (Exception e){
            e.printStackTrace();
        }
        return flag;
    }

    /**
     * 停止推流
     * @param source
     * @param rs
     */
    public void stopPush(String source,String rs){
       /* CommandTasker tasker = ConvertVideoProcessManager.get(ip);
        if(tasker != null){
            //结束推流
            process.destroy();
        }*/
        ConvertVideoProcessManager.removeAndClose(CommonUtil.getSourceKey(source,rs));
    }

    /**
     * 构造输入流地址
     * @param ip
     * @return
     */
    private String inputStreamPath(String ip){
        return "rtmp://"+ip+":1935/livetv/cctv6";
    }

    /**
     * 构造输出流地址
     * @param sourceKey
     * @return
     */
    private String outputStreamPath(String sourceKey){
        return "http://127.0.0.1:8081/rtsp/receive?sourceKey="+sourceKey;
    }
}
