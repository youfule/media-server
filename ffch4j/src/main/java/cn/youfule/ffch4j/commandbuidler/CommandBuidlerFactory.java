package cn.youfule.ffch4j.commandbuidler;

/**
 * 默认流式命令构建器工厂类
 * @author eguid
 *
 */
public class CommandBuidlerFactory {

	public static CommandBuidler createBuidler() {
		return new cn.youfule.ffch4j.commandbuidler.DefaultCommandBuidler();
	};
	
	public static  CommandBuidler createBuidler(String rootpath) {
		return new cn.youfule.ffch4j.commandbuidler.DefaultCommandBuidler(rootpath);
	};
}
