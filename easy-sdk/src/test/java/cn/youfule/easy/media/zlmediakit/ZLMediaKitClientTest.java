package cn.youfule.easy.media.zlmediakit;

import cn.youfule.easy.core.Result;
import cn.youfule.easy.media.zlmediakit.vo.MediaInfo;
import cn.youfule.easy.media.zlmediakit.vo.MediaStream;
import junit.framework.TestCase;

import java.util.List;

public class ZLMediaKitClientTest extends TestCase {

    public void testAddStreamProxy() {
        ZLMediaKitConfig config = new ZLMediaKitConfig();
        config.setLanHost("192.168.2.199");
        config.setLanHttpPort(8080);

        ZLMediaKitClient client = new ZLMediaKitClient(config);
        System.out.println(client.getThreadsLoad());
        Result<MediaStream> sp = client.addStreamProxy(new MediaStream("hunantv"),"rtmp://58.200.131.2:1935/livetv/hunantv",true,true,true,false,0);
        System.out.println(sp);
        Result<List<MediaInfo>> list = client.getMediaList(null,MediaStream.DEFAULT_VHOST,MediaStream.DEFAULT_APP,true);
        System.out.println(list);
    }

    public void testGetMediaList() {
        ZLMediaKitConfig config = new ZLMediaKitConfig();
        config.setLanHost("192.168.2.199");
        config.setLanHttpPort(8080);
        ZLMediaKitClient client = new ZLMediaKitClient(config);

        Result<List<MediaInfo>> list = client.getMediaList(null,MediaStream.DEFAULT_VHOST,MediaStream.DEFAULT_APP,true);
//        System.out.println(list);
    }

    public void testGetMediaInfo() {
        ZLMediaKitConfig config = new ZLMediaKitConfig();
        config.setLanHost("192.168.2.199");
        config.setLanHttpPort(8080);
        ZLMediaKitClient client = new ZLMediaKitClient(config);

        Result<MediaInfo> info = client.getMediaInfo("rtmp",new MediaStream("obs"));

    }
}