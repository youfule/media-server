package cn.youfule.easy.core;

public interface ActionCallback<T> {

	void action(T obj);
}
