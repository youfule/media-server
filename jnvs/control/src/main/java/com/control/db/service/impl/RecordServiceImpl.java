package com.control.db.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import com.control.db.dao.RecordRepository;
import com.control.db.entity.RecordEntity;
import com.control.db.service.RecordService;

@Service
public class RecordServiceImpl implements RecordService {
	@Autowired
	RecordRepository recordRepository;

	@Override
	public List<RecordEntity> listAll(RecordEntity recordEntity) {
		Example<RecordEntity> example = Example.of(recordEntity);
		List<RecordEntity> cameraOptional = recordRepository.findAll(example);
		return cameraOptional;
	}

	@Override
	public RecordEntity saveOrUpdate(RecordEntity recordEntity) {
		// TODO Auto-generated method stub
		return recordRepository.save(recordEntity);
	}

	@Override
	public RecordEntity findOne(RecordEntity recordEntity) {
		Example<RecordEntity> example = Example.of(recordEntity);
		Optional<RecordEntity> cameraOptional = recordRepository.findOne(example);
		if (cameraOptional.isPresent()) {
			RecordEntity cameraResult = cameraOptional.get();
			return cameraResult;
		} else {
			return null;
		}
	}

}
