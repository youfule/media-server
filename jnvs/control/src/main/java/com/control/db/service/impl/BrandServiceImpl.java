package com.control.db.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import com.control.db.dao.BrandRepository;
import com.control.db.entity.BrandEntity;
import com.control.db.service.BrandService;

@Service
public class BrandServiceImpl implements BrandService {
	@Autowired
	BrandRepository brandRepository;

	@Override
	public List<BrandEntity> listAll(BrandEntity brandEntity) {
		Example<BrandEntity> example = Example.of(brandEntity);
		List<BrandEntity> cameraOptional = brandRepository.findAll(example);
		return cameraOptional;
	}

	@Override
	public BrandEntity saveOrUpdate(BrandEntity brandEntity) {
		// TODO Auto-generated method stub
		return brandRepository.save(brandEntity);
	}

	@Override
	public BrandEntity findOne(BrandEntity brandEntity) {
		Example<BrandEntity> example = Example.of(brandEntity);
		Optional<BrandEntity> cameraOptional = brandRepository.findOne(example);
		if (cameraOptional.isPresent()) {
			BrandEntity cameraResult = cameraOptional.get();
			return cameraResult;
		} else {
			return null;
		}
	}

}
